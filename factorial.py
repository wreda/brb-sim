#!/usr/bin/env python

import itertools
import subprocess
import os
import sys

numClients = [18]
numServers = [9]
numWorkload = [1]
impl = ["rs", "model", "credits"]
workloadModel = ["trace"]
interArrivalModel = ["poisson"]
serverConcurrency = [4]
customerCostModel= ["deadline"]
deadlineModel = ["constant", "equalMaxCost", "uniformIncrCost"]
valueSizeModel = ["fb.pareto"]
starvPrevention = ["disabled"]
starvParam = [1000]
serviceTimeParam = [0.5]
serviceTimeModel = ["constant"]
timeVaryingDrift = [3]
# utilization = [0.4, 0.45]
utilization = [0.7]
replicationFactor = [3]
shadowReadRatio = [0.1]
cubicC = [0.000004]
cubicSmax = [10]
cubicBeta = [0.2]
rateInterval = [20]
concurrencyWeight = [150]
hysterisisFactor = [2]
selectionStrategy = ["C3"]
accessPatternModel = ["uniform"]
accessPatternParam = [1.3]
nwLatencyBase = [0.05]
nwLatencyMu = [0]
nwLatencySigma = [0]
simulationDuration = [5000000000]
seed = [449]
numRequests = [10000]
# expScenario = ["heterogenousStaticServiceTimeScenario"]
expScenario = ["base"]
# slowServerFraction = [0.3, 0.5, 0.7]
# slowServerSlowness = [0.8, 0.5, 0.3]
# intervalParam = [0]
# timeVaryingDrift = [0]
slowServerFraction = [0]
slowServerSlowness = [0]
intervalParam = [10]
batchSizeModel = ["random.expovariate"]
batchSizeParam = [2]
leaderBalanceModel = ["RR"]
workloadModel = ["trace"]
workloadTrace = ["../trace-processing/small_track_hit_batches_sorted2"]
ctrlInterval = [1000]
reqCostModel = ["ec2"]
reqCostParam = [0]
backpressure = True
expPrefix = "brb"
logLevel = "INFO"
# logFolder = "higher-utilization-restart" + uniqId
# logFolder = "paperSkewSweep" + uniqId

#if not os.path.exists(logFolder):
#        os.makedirs(logFolder)

LIST = [numClients,
        numServers,
        numWorkload,
        impl,
        workloadModel,
        interArrivalModel,
        serverConcurrency,
        customerCostModel,
        deadlineModel,
        valueSizeModel,
        starvPrevention,
        starvParam,
        serviceTimeParam,
        utilization,
        serviceTimeModel,
        replicationFactor,
        shadowReadRatio,
        cubicC,
        cubicSmax,
        cubicBeta,
        rateInterval,
        concurrencyWeight,
        hysterisisFactor,
        selectionStrategy,
        accessPatternModel,
        accessPatternParam,
        nwLatencyBase,
        nwLatencyMu,
        nwLatencySigma,
        simulationDuration,
        seed,
        numRequests,
        expScenario,
        slowServerFraction,
        slowServerSlowness,
        intervalParam,
        timeVaryingDrift,
        batchSizeModel,
        batchSizeParam,
        workloadModel,
        workloadTrace,
        ctrlInterval,
        reqCostModel,
        reqCostParam,
        ]
PARAM_COMBINATIONS = list(itertools.product(*LIST))

#print len(PARAM_COMBINATIONS)

basePath = os.getcwd()

for combination in PARAM_COMBINATIONS:
        numClients, numServers, numWorkload, \
            impl, workloadModel, interArrivalModel, serverConcurrency, \
            customerCostModel, deadlineModel, \
            valueSizeModel, starvPrevention, \
            starvParam, serviceTime, utilization, \
            serviceTimeModel, replicationFactor, \
            shadowReadRatio, cubicC, cubicSmax, \
            cubicBeta, rateInterval, concurrencyWeight, \
            hysterisisFactor, selectionStrategy, accessPatternModel, \
            accessPatternParam, nwLatencyBase, \
            nwLatencyMu, nwLatencySigma, \
            simulationDuration, seed, \
            numRequests, expScenario, \
            slowServerFraction, \
            slowServerSlowness, intervalParam, \
            timeVaryingDrift, batchSizeModel, \
            batchSizeParam, workloadModel, \
            workloadTrace,  ctrlInterval, \
            reqCostModel, reqCostParam = combination
        folderStructure = tuple(x for x in combination if x!=workloadTrace)
        logFolder = '_'.join(map(lambda x : str(x), folderStructure))
        logFolder = logFolder.replace('[', '').replace(']', '').replace(', ', '_')
        logFolder = "logs/" + logFolder
        cmd = "python experiment.py \
                --numClients %s \
                --numServers %s \
                --numWorkload %s \
                --impl %s \
                --workloadModel %s \
                --interArrivalModel %s \
                --serverConcurrency %s \
                --customerCostModel %s \
                --deadlineModel %s \
                --valueSizeModel %s \
                --starvPrevention %s \
                --starvParam %s \
                --serviceTimeParam %s \
                --utilization %s \
                --serviceTimeModel %s \
                --replicationFactor %s \
                --shadowReadRatio %s \
        	--cubicC %s \
        	--cubicSmax %s \
        	--cubicBeta %s \
        	--rateInterval %s \
        	--concurrencyWeight %s \
        	--hysterisisFactor %s \
        	--selectionStrategy %s \
                --accessPatternModel %s \
                --accessPatternParam %s \
                --nwLatencyBase %s \
                --nwLatencyMu %s \
                --nwLatencySigma %s \
                --expPrefix %s \
                --simulationDuration %s \
                --seed %s \
                --numRequests %s \
                --expScenario %s \
                --slowServerFraction %s \
                --slowServerSlowness %s \
                --intervalParam %s \
                --timeVaryingDrift %s \
                --batchSizeModel %s \
                --batchSizeParam %s \
                --workloadModel %s \
                --workloadTrace %s \
                --ctrlInterval %s \
                --reqCostModel %s \
                --reqCostParam %s \
                --logFolder %s \
                --logLevel %s"\
                  % (numClients,
                     numServers,
                     numWorkload,
                     impl,
                     workloadModel,
                     interArrivalModel,
                     serverConcurrency,
                     customerCostModel,
                     deadlineModel,
                     valueSizeModel,
                     starvPrevention,
                     starvParam,
                     serviceTime,
                     utilization,
                     serviceTimeModel,
                     replicationFactor,
                     shadowReadRatio,
        	     cubicC,
        	     cubicSmax,
        	     cubicBeta,
        	     rateInterval,
        	     concurrencyWeight,
        	     hysterisisFactor,
        	     selectionStrategy,
                     accessPatternModel,
                     accessPatternParam,
                     nwLatencyBase,
                     nwLatencyMu,
                     nwLatencySigma,
                     deadlineModel,
                     simulationDuration,
                     seed,
                     numRequests,
                     expScenario,
                     slowServerFraction,
                     slowServerSlowness,
                     intervalParam,
                     timeVaryingDrift,
                     batchSizeModel,
                     batchSizeParam,
                     workloadModel,
                     workloadTrace,
                     ctrlInterval,
                     reqCostModel,
                     reqCostParam,
                     logFolder,
                     logLevel)
	if(backpressure and impl == "rs"):
		cmd += " --backpressure"
        r_cmd = "Rscript factorialResults.r %s %s" % (logFolder, deadlineModel)
        toPrint = "cd brb && mkdir -p " + logFolder + " && cd simulator && " + cmd + " > ../%s/%s_summary.txt 2>&1;"%(logFolder,deadlineModel)
        
        print toPrint
        ## run it ##
#         p = subprocess.Popen(cmd, shell=True, stderr=subprocess.PIPE)
#          
#         while True:
#             out = p.stderr.read(1)
#             if out == '' and p.poll() != None:
#                 break
#             if out != '':
#                 sys.stdout.write(out)
#                 sys.stdout.flush()







