The dependencies can be satisfied by first running these commands in R:
install.packages("ggplot2")
install.packages("data.table")
install.packages("Hmisc")

- First, we add our outputted logs to the log folder (This should be automatically handled if you're directly using the log files generated from an experiment)

- Next, from the command-line, run:

	$: Rscript plotting.r Folder Prefix 
	- Folder is the relative path of the plots from repo's logs directory
	  (e.g. for logs files located in ~/brb/logs/first_experiment
	- Prefix refers to the name prepended before the experiment logs
	  (e.g. for a file named 'test_vQueueLen' our prefix is 'test')

- The resulting graphs will be produced in the plots directory. So far, we produce the following graphs:
	- vQueue Length timeseries
	- Request cost ECDF
	- Request Response Time ECDF
	- Task Response Time ECDF
	- FRG per replica per client
	- Aggregate FRG (averaged over all clients)

Additional notes:
- If you need to compare read latencies between different deadline assignment strategies, you can utilize the comparison.r file to do so. You'll need to perform the following steps:
	1) Group the "taskLatency" logs in a single folder
	2) Make sure the "taskLatency" logs are prepended with the appropriate deadline model name
		- E.g. For an "equalMaxCost" deadline model, the log should be named "equalMaxCost_taskLatency"
	3) Run the following command:
		$: Rscript comparison.r Folder
		# Similarly to plotting.r, Folder is also the relative path to the folder
		# containting the logs

- If you're running experiments using factorial, you can also utilize the factorial.r script to automatically generate plots for all experiments using both the plotting.r and comparison.r scripts.
