exps = dir('../logs')

#Read experiment metadata
exp_params = data.frame()
dir.create(file.path('../plots/comparisons'))

for (x1 in 1:length(exps))
{
    #Read experiment metadata
	e <- read.table(paste("../logs/", exps[[x1]], "/InputParams", sep=""), header=T)
	folder <- paste(exps[x1], '/', sep="")
    e$folder <- rep(folder,nrow(e))
    exp_params <- rbind(exp_params, e)
    #Run plots for single experiments
	prefix <- paste(e$deadlineModel, sep="")
	source('plotting.r')
}

#Add a unique ID for each experiment
exp_params$ID<-seq.int(nrow(exp_params))

#Group experiments according to impl and deadlineModel
splitList = list()
count = 1
for (x2 in 1:ncol(exp_params))
{
	if(!(x2 %in% which(names(exp_params) %in% c("impl", "deadlineModel", "expPrefix", "folder", "ID", "logFolder", "seed"))))
	{
		splitList[[count]] <- exp_params[,x2]
		count = count+1
	}
}

exp_params_s <- split(exp_params, splitList)
for(i in 1:length(exp_params_s))
{
	if(nrow(exp_params_s[[i]])>0){
	#Run comparison plots for task Latencies
	exp_params_g <- exp_params_s[[i]]
	source('comparison.r')
	}
}
