#!/usr/bin/env Rscript

require(ggplot2)
require(data.table)
require(Hmisc)
args <- commandArgs(trailingOnly = TRUE)
if(length(args) > 0)
{
	folder <- args[1]	
	prefix <- args[2]
}
#else -> we're running this using factorial.r, use the passed values
#print(folder)

folder <- paste(folder, "/", sep="")

if (file.exists(paste("../logs/", folder, prefix, "_taskLatency", sep="")))
{
taskLatency <- read.table(paste("../logs/", folder, prefix, "_taskLatency", sep=""))
colnames(taskLatency)[1] <- "ClientId"
colnames(taskLatency)[2] <- "Timestamp"
colnames(taskLatency)[3] <- "Latency"
colnames(taskLatency)[4] <- "ServerId"

requestLatency <- read.table(paste("../logs/", folder, prefix, "_requestLatency", sep=""))
colnames(requestLatency)[1] <- "ClientId"
colnames(requestLatency)[2] <- "Timestamp"
colnames(requestLatency)[3] <- "ServerId"
colnames(requestLatency)[4] <- "Latency"

vQueueLen <- read.table(paste("../logs/", folder, prefix, "_vQueueLen", sep=""))
colnames(vQueueLen)[1] <- "RG"
colnames(vQueueLen)[2] <- "Timestamp"
colnames(vQueueLen)[3] <- "Length"

# May not be available depending on utilized deadline strategy
frgAvailable = file.info(paste("../logs/", folder, prefix, "_frg", sep=""))$size>0

if(frgAvailable)
{
frg <- read.table(paste("../logs/", folder, prefix, "_frg", sep=""))
colnames(frg)[1] <- "ClientId"
colnames(frg)[2] <- "Timestamp"
colnames(frg)[3] <- "RG"
colnames(frg)[4] <- "Range"
colnames(frg)[5] <- "Expectation"
}

requestCost <- read.table(paste("../logs/", folder, prefix, "_requestCost", sep=""))
colnames(requestCost)[1] <- "RG"
colnames(requestCost)[2] <- "Cost"

act.mon <- read.table(paste("../logs/", folder, prefix, "_ActMon", sep=""))
colnames(act.mon)[1] <- "ServerId"
colnames(act.mon)[2] <- "Timestamp"
colnames(act.mon)[3] <- "ActiveRequests"

pending.requests <- read.table(paste("../logs/", folder, prefix, "_PendingRequests", sep=""))
colnames(pending.requests)[1] <- "ClientId"
colnames(pending.requests)[2] <- "Timestamp"
colnames(pending.requests)[3] <- "ServerId"
colnames(pending.requests)[4] <- "PendingRequests"

# Create plot directory

dir.create(file.path('../plots/', folder))

# Plot bar chart for average server utilization
# Plot the Aggregate FRG means & SDs
act.mon <- as.data.frame(as.list(aggregate(ActiveRequests ~ ServerId, FUN=mean, data=act.mon)))
g <- ggplot(act.mon, aes(x=ServerId, y=ActiveRequests)) + geom_bar(stat="identity") + labs(x="ServerId", y="Average Occupancy") + ggtitle("Server utilization")
ggsave(g, file=paste('../plots/', folder, prefix, "_activeMon.pdf", sep=""), width=15, height=10)

# Plot Queue Sizes per Replica Group
# get the range for the x and y axis 
xrange <- range(vQueueLen$Timestamp) 
yrange <- range(vQueueLen$Length)
nRGs <- max(vQueueLen$RG)
colors <- rainbow(nRGs)
linetype = c(rep(1, nRGs))
vQueueLen <- split(vQueueLen, list(vQueueLen$RG))
print(paste('../plots/', folder, prefix, "_vQueueLen.pdf", sep=""))
pdf(file=paste('../plots/', folder, prefix, "_vQueueLen.pdf", sep=""))
plot(xrange,yrange,type="n", xlab="Timestamp", ylab="Length")
for(i in 1:length(vQueueLen)) {
if(nrow(vQueueLen[[i]])>0){
	lines(vQueueLen[[i]]$Timestamp, vQueueLen[[i]]$Length, type="l",lwd=1.5, col=colors[i])
	}
}
# add a title and subtitle 
title("Virtual Queue Size")
# add a legend 
legend(xrange[1], yrange[2], 0:(nRGs-1), cex=0.8, col=colors, lty=linetype, title="RGs")
dev.off()

if(frgAvailable)
{
	# Plot FRG value means & SDs for all clients
	splitfrg <- split(frg, list(frg$ClientId))
	for(i in 1:length(splitfrg)) {
	if(nrow(splitfrg[[i]])>0){
		clientfrg <- splitfrg[[i]]
		clientfrg <- split(clientfrg, list(clientfrg$RG))
		g <- ggplot()
		for(i in 1:length(clientfrg)) {
			if(nrow(clientfrg[[i]])>0){
				replicafrg <- clientfrg[[i]]
				replicafrg <- as.data.frame(as.list(aggregate(Expectation ~ Range + RG + ClientId, FUN=function(x) c(mean =mean(x), sd=sd(x) ), data=replicafrg)))
				replicafrg[is.na(replicafrg)] <- 0
				g <- g + geom_line(data=replicafrg, colour=i, linetype="solid", size=0.5, aes(x=Range, y=Expectation.mean, group=RG, color=RG)) + ggtitle("Expected Queue Delay")
			}
		}
		ggsave(g, file=paste('../plots/', folder, prefix, "Client", toString(replicafrg$ClientId[1]), "_frg.pdf", sep=""), width=15, height=10)
		}
	}
	
	# Plot the Aggregate FRG means & SDs
	aggfrg <- as.data.frame(as.list(aggregate(Expectation ~ Range + RG, FUN=function(x) c(mean =mean(x), sd=sd(x) ), data=frg)))
	aggfrg[is.na(aggfrg)] <- 0
	g <- ggplot(data=aggfrg, aes(x=Range, y=Expectation.mean)) + geom_line(linetype="solid", size=0.5, aes( group=RG, color=RG)) + ggtitle("Aggregate Expected Queue Delay")
	ggsave(g, file=paste('../plots/', folder, prefix, "_aggfrg.pdf", sep=""), width=15, height=10)
}
pdf(file=paste('../plots/', folder, prefix, "_requestCost.pdf", sep=""))
Ecdf(requestCost[,2], xlab="Request Cost (bytes)")
dev.off()
pdf(file=paste('../plots/', folder, prefix, "_requestLatency.pdf", sep=""))
Ecdf(requestLatency[,4], xlab="Request Latency (microseconds)")
dev.off()
pdf(file=paste('../plots/', folder, prefix, "_taskLatency.pdf", sep=""))
Ecdf(taskLatency[,3], xlab="Task Latency (microseconds)")
dev.off()
}
