library(ggplot2)
library(reshape2)
library("grid")
library("xlsx")

results <- data.frame()
rtrs <- data.frame()
count = 0

for(i in 1:nrow(exp_params_g)) {
	folder <- exp_params_g[i,]$folder
	strategy <- exp_params_g[i,]$deadlineModel
	impl <- exp_params_g[i,]$impl
	seed <- exp_params_g[i,]$seed
	count = count+1
	if (file.exists(paste("../logs/", folder, strategy, "_taskLatency", sep=""))){
	taskLatency <- read.table(paste("../logs/", folder, strategy, "_taskLatency", sep=""))
	colnames(taskLatency)[1] <- "ClientId"
	colnames(taskLatency)[2] <- "Timestamp"
	colnames(taskLatency)[3] <- "Latency"
	colnames(taskLatency)[4] <- "ServerId"
	taskLatency$strategy <- rep(strategy,nrow(taskLatency))
	taskLatency$impl <- rep(impl,nrow(taskLatency))
	taskLatency$seed <- rep(seed, nrow(taskLatency))
	taskLatency$ID <- rep(exp_params_g[i,]$ID,nrow(taskLatency))
	results <- rbind(results, taskLatency)
	}
	if (file.exists(paste("../logs/", folder, strategy, "_rtrSD", sep="")))
	{
	rtrSD <- read.table(paste("../logs/", folder, strategy, "_rtrSD", sep=""))
	colnames(rtrSD)[1] <- "ClientId"
	colnames(rtrSD)[2] <- "Timestamp"
	colnames(rtrSD)[3] <- "SD"
	rtrSD$strategy <- rep(strategy,nrow(rtrSD))
	rtrSD$impl <- rep(impl,nrow(rtrSD))
	rtrSD$seed <- rep(seed, nrow(rtrSD))
	rtrSD$ID <- rep(exp_params_g[i,]$ID,nrow(rtrSD))
	rtrs <- rbind(rtrs, rtrSD)		
	}
}

#aggregate rtrs by their mean
rtrs <- aggregate(SD ~ strategy + impl, data=rtrs, FUN=mean)

#produce rtrSD boxplot
ylim=c(0,10)
exp <- subset(exp_params_g, select = -c(deadlineModel, impl, logFolder, folder, expPrefix, workloadTrace, seed))
filename = do.call(paste, c(as.list(exp), sep="_"))
g <- ggplot(rtrs, aes(strategy, SD, fill=impl, width=0.5)) + theme(title = element_text(size=25), axis.text = element_text(size=25), legend.title = element_text(size=25), legend.margin = unit(20, "mm"), plot.title = element_text(size=20), legend.text = element_text(size=20), legend.key.size=unit(20, "mm")) + geom_bar(position="dodge", stat="identity") + coord_flip() +
ggtitle(paste("Intrabatch Relative SD (Cost Derivative:", exp$customerCostModel, ')')) + ylab("Relative SD (%)") 
ggsave(g, file=paste("../plots/comparisons/","rtrSD_comparison_", filename, ".pdf", sep=""), width=15, height=10)

#produce latency boxplot
ylim=c(0,600)
colors <- c('red', 'blue', 'orange', 'green', 'violet')[unique(results$impl)]
exp <- subset(exp_params_g, select = -c(deadlineModel, impl, logFolder, folder, expPrefix, workloadTrace, seed))
filename = do.call(paste, c(as.list(exp), sep="_"))
pdf(file=paste("../plots/comparisons/","latency_comparison_", filename, ".pdf", sep=""))
op <- par(cex.axis=0.85, mar = c(5, 10, 4, 2) + 0.1)
temp <- boxplot(Latency~impl*strategy,results,col=colors,xlab="Latency (ms)",ylab=" ",main="Task Response Times",outline=FALSE,las = 2, horizontal=TRUE)
#text(seq(along = temp$n), temp$stats[5, ],labels = temp$stats[5,], pos = 1, cex=0.2)
dev.off()
#reset plot parameters
par(op)

#print smoe stuff to an xls spreadsheet
results_s <- split(results, list(results$seed))
results_xls <- data.frame()
for(j in 1:length(results_s))
{
	if(nrow(results_s[[j]])>0){
	result <- results_s[[j]]
	result_agg <- as.data.frame(as.list(aggregate(Latency ~ strategy + impl + seed, FUN=function(x) quantile(x, probs = c(0.5, 0.95, 0.99)), data=result)))
	results_xls <- rbind(results_xls, result_agg)	
	}	
}
#compute averages of quantiles
results_xls <- aggregate(cbind(Latency.50., Latency.95., Latency.99.)~strategy+impl, data=results_xls, mean, na.rm=TRUE)
#reshape table
results_xls <- melt(results_xls, id.vars =c("strategy", "impl"))
results_xls <- dcast(results_xls, strategy ~ impl + variable)
#calculate model to credits ratios
#FIXME generalize script to other implementation models
results_xls$MC_ratio_50 <- results_xls$model_Latency.50. / results_xls$credits_Latency.50.
results_xls$MC_ratio_95 <- results_xls$model_Latency.95. / results_xls$credits_Latency.95.
results_xls$MC_ratio_99 <- results_xls$model_Latency.99. / results_xls$credits_Latency.99.

exp <- subset(exp_params_g, ID == results[1,]$ID)
exp <- subset(exp, select = -c(impl, logFolder, folder, expPrefix, workloadTrace, seed))
filename = paste('../plots/comparisons/', 'percentile_data_', do.call(paste, c(as.list(exp), sep="_")), ".xlsx", sep="")
write.xlsx(x = results_xls, file = filename, sheetName = "Latency_comparisons", row.names = FALSE)

results_s <- split(results, list(results$strategy))
for(j in 1:length(results_s))
{
	if(nrow(results_s[[j]])>0){
	result <- results_s[[j]]
	exp <- subset(exp_params_g, ID == result[1,]$ID)
	exp <- subset(exp, select = -c(impl, logFolder, folder, expPrefix, workloadTrace, seed))
    filename = do.call(paste, c(as.list(exp), sep="_"))
    g <- ggplot(result, aes(x = Latency)) + theme(title = element_text(size=25), axis.text = element_text(size=25), legend.title = element_text(size=25), legend.margin = unit(20, "mm"), plot.title = element_text(size=20), legend.text = element_text(size=20), legend.key.size=unit(20, "mm")) + stat_ecdf(aes(group = impl, colour = impl)) + ggtitle(paste("Latency CDF (Strategy:", exp$deadlineModel, ", Cost Derivative:", exp$customerCostModel, ')')) 
	ggsave(g, file=paste('../plots/comparisons/' , "ecdf_comparison_", filename, ".pdf", sep=""), width=15, height=10)
	}
}