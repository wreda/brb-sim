'''
Created on 09 Mar 2015

@author: reda
'''

import SimPy.Simulation as Simulation
import numpy
import server
import client
import workload

class StatCollector(Simulation.Process):
    '''
    Periodically collects parameters from clients and servers
    '''
    def __init__(self, numRequests, clients, servers, serversByReplicaGroup,
                 workloads, replicaGroupNum, starvPrevention, starvThreshold,
                 deadlineModel, workloadModel, log):
        self.numRequests = numRequests
        self.clients = clients
        self.servers = servers
        self.serversByReplicaGroup = serversByReplicaGroup
        self.workloads = workloads
        self.replicaGroupNum = replicaGroupNum
        self.starvPrevention = starvPrevention
        self.starvThreshold = starvThreshold
        self.deadlineModel = deadlineModel
        self.reqResDiff = Simulation.Monitor(name="ReqResDiffMonitor")
        self.workloadModel = workloadModel
        # Virtual queue size monitors
        self.vQueueLenMonitor = {rg:Simulation.Monitor(name="vQueueLenMonitor") for rg in range(replicaGroupNum)}
        self.vQueueDLMonitor = {rg:Simulation.Monitor(name="vQueueDLMonitor") for rg in range(replicaGroupNum)}
        self.vQueueVarMonitor = Simulation.Monitor(name="vQueueVarMonitor")
        self.log = log
        Simulation.Process.__init__(self, name='StatCollector')
        
    def run(self, delay):
        info = self.log.info
        percCompletion = 10
        while True:
            clientsFinished = True
            totalResRecv = 0
            totalReqArrived = 0
            vQueueLengths = {rg:0 for rg in range(self.replicaGroupNum)}
            vQueueDeadlines = {rg:-1 for rg in range(self.replicaGroupNum)}
            for c in self.clients:
                if(self.deadlineModel == "greedyDynamicCost"):
                    c.update_frg_greedy()
                elif(self.deadlineModel == "curvefitDynamicCost"):
                    c.frg_curvefit_update()
                totalResRecv += c.taskResponsesReceived
                totalReqArrived += c.taskRequestsArrived
                if(not c.isDepleted()):
                    clientsFinished = False
                
                #If we're using credits/model implementations, queues are located at the clients
                if((isinstance(c, client.Client) or isinstance(c, client.CreditsClient)) and not isinstance(c, client.ReplicaLeaderClient)):
                    for rg in self.serversByReplicaGroup.iterkeys():
                        qlen, head_deadline = c.peek_queue(rg)
                        vQueueLengths[rg] += qlen
                        if(qlen>0):
                            vQueueDeadlines[rg] = max(vQueueDeadlines[rg], head_deadline)
                        # FIXME
                        if(self.starvPrevention != 'disabled'):
                            if(isinstance(c, client.CreditsClient)):
                                self.prioritizeStarved(c.executors[rg].backlogQueue)
                            elif(not (self.isinstance(c, client.ReplicaLeaderClient))):
                                self.prioritizeStarved(c.customerQueues[rg])
            #If I'm using the credits/leader model, queues are located at the servers
            if(isinstance(self.clients[0], client.ReplicaLeaderClient)\
               or isinstance(self.clients[0], client.CreditsClient)):
                for rg in self.serversByReplicaGroup.iterkeys():
                    for s in self.serversByReplicaGroup[rg]:
                        qlen, head_deadline = s.peek_queue(rg)
                        vQueueLengths[rg] += qlen
                        if(qlen>0):
                            vQueueDeadlines[rg] = max(vQueueDeadlines[rg], head_deadline)
                        # FIXME
                        if(self.starvPrevention != 'disabled'):
                            self.prioritizeStarved(s.replicaQueues[rg])
            for rg in self.vQueueLenMonitor:
                self.vQueueLenMonitor[rg].observe(vQueueLengths[rg])
                self.vQueueDLMonitor[rg].observe(vQueueDeadlines[rg])
            varVQueueLen = numpy.var(vQueueLengths.values())
            self.vQueueVarMonitor.observe(varVQueueLen)
            if((float(totalResRecv)/self.numRequests*100.0) >= percCompletion):
                percCompletion += 10
                info('Simulation run %d percent complete!'%(float(totalResRecv)/self.numRequests*100.0))
                info('Simulation time: %s'%Simulation.now())
            if(clientsFinished and workload.finished()):
                #We're done here!
                for c in self.clients:
                    c.deactivate()
                return
            yield Simulation.hold, self, delay

    # FIXME: this is wrong. We need to reimplment this
    def prioritizeStarved(self, queues):
        #Check queued requests
        #set starved request deadlines to 0
        for tup in queues:
            if(self.starvPrevention == "constant"):
                threshold = self.starvThreshold
            elif(self.starvPrevention == "dynamic"):
                threshold = tup[2].frg
            if (Simulation.now() - tup[2].systemArrivalTime) > threshold:
                tup[2].deadline = 0
                newTup = (0, tup[1], tup[2])
                tupIndex = queues.index(tup)
                queues[tupIndex] = newTup         
        