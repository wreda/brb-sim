import SimPy.Simulation as Simulation
import random
import numpy
import md5
import constants
import logger
import math
import sys



class Request():
    """A simple Request. Applications may subclass this
       for holding specific attributes if need be"""
    def __init__(self, fullId, parentId, key, batchsize,
                 sendingClient, latencyMonitor, cost=1):
        self.id = fullId    # Parent request + subrequest Id
        self.parentId = parentId       # Parent request Id
        self.key = key # key on the token ring
        self.batchsize = batchsize
        self.sendingClient = sendingClient
        self.start = Simulation.now()
        self.completionEvent = Simulation.SimEvent("ClientToServerCompletion")
        self.cost = cost
        self.systemArrivalTime = None
        self.sendTime = None
        self.latencyMonitor = latencyMonitor
        self.replicaGroup = None

    # Used as a notifier mechanism
    def sigRequestComplete(self, piggyBack=None):
        if (self.completionEvent is not None):
            self.completionEvent.signal(piggyBack)

class Workload(Simulation.Process):

    def __init__(self, id_, latencyMonitor, clientList,
                 model, model_param, numRequests, batchSizeModel,
                 batchSizeParam, valueSizeList, server):
        self.id_ = id_
        self.log = logger.getLogger("workload%d" % id_, constants.LOG_LEVEL)
        self.latencyMonitor = latencyMonitor
        self.clientList = clientList
        self.model = model
        self.model_param = model_param
        self.numRequests = numRequests
        self.batchSizeModel = batchSizeModel
        self.batchSizeParam = batchSizeParam
        self.valueSizeList = valueSizeList
        self.server = server
        Simulation.Process.__init__(self, name='Workload' + str(id_))

    # TODO: also need non-uniform client access
    # Need to pin workload to a client
    def run(self):
        
        requestCounter = 0
        while(self.numRequests != 0):
            yield Simulation.hold, self,

            batchsize = 1
            if (self.batchSizeModel == "constant"):
                batchsize = self.batchSizeParam
            else:
                print "Invalid batch-size-model"
                assert False

            # Push out a request...
            clientNode = None
            subRequestCounter = 0

            parentRequestId = "%s-Request%s" % (self.id_,
                                          str(requestCounter))
            fullId = "%s-%s" % (parentRequestId, str(subRequestCounter))

            # TODO: lalith to add consisten hashing based on the workloadSC
            cost = 1
            req = Request(fullId=fullId,
                                parentId=parentRequestId,
                                key=None,
                                batchsize=batchsize,
                                sendingClient=clientNode,
                                latencyMonitor=self.latencyMonitor,
                                cost=cost)

            self.server.enqueue(req)
            self.numRequests -= 1
            requestCounter += 1
            subRequestCounter += 1

            # Simulate inter-arrival times for requests
            if (self.model == "poisson"):
                yield Simulation.hold, self,\
                    numpy.random.poisson(self.model_param)
            else:
                print "Invalid workload inter-arrival time model"
                assert False



class Server():
    """A representation of a physical server that holds resources"""
    def __init__(self, id_, resourceCapacity,
                 serviceTime, serviceTimeModel, log):
        self.id = id_
        self.log = log
        self.serviceTime = serviceTime
        self.serviceTimeModel = serviceTimeModel
        self.queueResource = Simulation.Resource(capacity=resourceCapacity,
                                                 monitored=True)
        self.serverRRMonitor = Simulation.Monitor(name="ServerMonitor")

    def enqueue(self, req):
        executor = Executor(self, req)
        self.serverRRMonitor.observe("%s %s" % (1, req.id))
        Simulation.activate(executor, executor.run(), Simulation.now())

    def getServiceTime(self):
        serviceTime = 0.0
        if (self.serviceTimeModel == "random.expovariate"):
            serviceTime = random.expovariate(1.0/(self.serviceTime))
        else:
            print "Unknown service time model"
            sys.exit(-1)

        return serviceTime


class Executor(Simulation.Process):

    def __init__(self, server, req):
        self.server = server
        self.req = req
        Simulation.Process.__init__(self, name='Executor')

    def run(self):
        start = Simulation.now()
        yield Simulation.hold, self
        yield Simulation.request, self, self.server.queueResource
        waitTime = Simulation.now() - start         # W_i
        serviceTime = self.server.getServiceTime()  # Mu_i
        yield Simulation.hold, self, serviceTime
        yield Simulation.release, self, self.server.queueResource

        # self.server.log.info("req %s serviceTime %f" % (self.req.id, serviceTime))
        self.req.latencyMonitor.observe("%s  %s" %
                                       (Simulation.now() - self.req.start,
                                        self.req.id))



def runTest():
    random.seed(445)
    numpy.random.seed(445)
    log = logger.getLogger("Test", logging.INFO)

    Simulation.initialize()

    avgSysRate = 1
    arrivalRate = 0.9

    serv = Server(id_=1,
                         resourceCapacity=1,
                         serviceTime=1/avgSysRate,
                         serviceTimeModel="random.expovariate",
                         log=log)

    latencyMonitor = Simulation.Monitor(name="Latency")

    w = Workload(id_=1, latencyMonitor=latencyMonitor,
                              clientList=[],
                              model="poisson",
                              model_param=1/arrivalRate,
                              numRequests=10000,
                              batchSizeModel="constant",
                              batchSizeParam=1,
                              valueSizeList=[],
                              server=serv)
    Simulation.activate(w, w.run(), at=0.0),

    Simulation.simulate(until=10**6)

    log.info("------- Latency ------")
    log.info("Mean Latency: %f" %\
        (sum([float(entry[1].split()[0]) for entry in latencyMonitor])\
        / float(len(latencyMonitor))))
    log.info("Median Latency: %f" %\
        (numpy.median(numpy.array([float(entry[1].split()[0]) for entry in latencyMonitor]))))
    log.info("95 percentile Latency: %f" %\
        (numpy.percentile(numpy.array([float(entry[1].split()[0]) for entry in latencyMonitor]), 95)))
    log.info("99 percentile Latency: %f" %\
        (numpy.percentile(numpy.array([float(entry[1].split()[0]) for entry in latencyMonitor]), 99)))
 
    # As a sanity check, we compute the mean, median, 95, and 99 percentile of waiting time
    # of a M/M/1 queueing system with the same arrival rate and with a service rate
    # equal to the system aggregate service rate
    def perc_waiting_time(mean, r):
        return mean * math.log(100 / (100 - r))

    mean = 1 / (avgSysRate - arrivalRate)
    p50 = perc_waiting_time(mean, 50)
    p95 = perc_waiting_time(mean, 95)
    p99 = perc_waiting_time(mean, 99)
    log.info("------- M/M/1 queuing system analysis -------")
    log.info("Mean Latency: %f" % mean)
    log.info("Median Latency: %f" % p50)
    log.info("95 percentile Latency: %f" % p95)
    log.info("99 percentile Latency: %f" % p99)

if __name__ == '__main__':
    import logging
    logger.init(constants.LOG_FILE, logging.INFO)
    runTest()



