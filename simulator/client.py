import SimPy.Simulation as Simulation
import random
import numpy
import heapq
import constants
import logger
import ewma
import math
import copy
import ratelimiter
import controller
import workload
import server
import operator
import warnings
import request

#Ignore numpy warnings
warnings.simplefilter('ignore', numpy.RankWarning)
from collections import defaultdict
from yunomi.stats.exp_decay_sample import ExponentiallyDecayingSample

class Client():
    def __init__(self, id_, latencyMonitor, serverList, serversByReplicaGroup, replicationFactor,
                 shadowReadRatio, demandWeight, deadlineModel, serviceTime, cubicC, cubicSmax,
                 cubicBeta, rateInterval, concurrencyWeight, hysterisisFactor, selectionStrategy,
                 backpressure, costExponent):
        self.id = id_
        self.log = self.init_logger(id_)
        self.log.debug("Create client %d" % id_)
        self.serverList = serverList
        self.serversByReplicaGroup = serversByReplicaGroup
        self.replicationFactor = replicationFactor
        self.deadlineModel = deadlineModel
        
        # C3 params (passed here for generalization purposes)
        self.cubicC = cubicC
        self.cubicSmax = cubicSmax
        self.cubicBeta = cubicBeta
        self.rateInterval = rateInterval
        self.concurrencyWeight = concurrencyWeight
        self.hysterisisFactor = hysterisisFactor
        self.REPLICA_SELECTION_STRATEGY = selectionStrategy
        self.backpressure = backpressure
        self.costExponent = costExponent
        
        # Put it in the schedule
        self.latencyMonitor = latencyMonitor
        self.pendingRequestsMonitor = \
            Simulation.Monitor(name="PendingRequests")
        self.requestLatencyMonitor = Simulation.Monitor(name="requestLatencyMonitor")
        self.intraBatchRTRSDMonitor = Simulation.Monitor(name="intraBatchRTRSDMonitor")
        self.greedyNonMonotonicityMonitor = Simulation.Monitor(name="monotonicityMonitor")
        self.frgMonitor = Simulation.Monitor(name="frgMonitor")
        self.frgVarMonitor = Simulation.Monitor(name = "frgVarMonitor")
        self.shadowReadRatio = shadowReadRatio
        self.demandWeight = demandWeight
        self.taskRequestsArrived = 0
        self.taskResponsesReceived = 0
        self.reqResponsesReceived = 0
        self.falseBottleneckEstCount = 0

        #Temporarily using this to assess effectiveness of aggregating server response times
        self.serviceTime = serviceTime

        # Book-keeping and metrics to be recorded follow...

        # Number of outstanding requests at the client
        self.pendingRequestsMap = {node: 0 for node in serverList}

        # Number of outstanding requests times oracle-service time of replica
        self.pendingXserviceMap = {node: 0 for node in serverList}
        
        # Track the workload per replica group over a controller interval
        self.workloadPerRG = {rg: 0 for rg in serversByReplicaGroup.iterkeys()}

        # Used to track request response time from the perspective of the client
        self.requestBatchCounter = {}
        
        # Used to track request response times from the perspective of the client
        self.requestCompletionTimes = {}
        
        # Used to add delays to our deadline in order to account for queuing delays (still experimental)
        self.delay = {ci: 1 for ci in range(len(serverList))}
        
        # Replica group queues
        self.customerQueues = {rg: list() for rg in serversByReplicaGroup.iterkeys()}
        
        #Adaptive parameters for dynamic, newDynamic, greedy and curve-fitting deadline strategies
        
        #Depletion rate parameters
        self.depl_rate_map = {rg: 1 for rg in serversByReplicaGroup.iterkeys()}
        self.last_update_depl_rate_time = Simulation.now()
        
        #Expected queueing delay EWMAs  
        self.ewma_points = numpy.arange(0,3,0.1)
        initial_ewma_points = {n: ewma.EWMA() for n in range(len(self.ewma_points))}
        self.ewmas = {rg: copy.deepcopy(initial_ewma_points) for rg in serversByReplicaGroup.iterkeys()}
        
        #Greedy FRG map
        initial_frg_greedy_points = {n : 0 for n in range(len(self.ewma_points))}
        self.frg_greedy_map = {rg: initial_frg_greedy_points.copy() for rg in serversByReplicaGroup.iterkeys()}

        #Curve-fitting map
        self.polyfunc = {rg: numpy.poly1d(0) for rg in serversByReplicaGroup.iterkeys()}

    def __repr__(self):
        return "Client%d" % self.id

    def init_logger(self, id_):
        return logger.getLogger("Client%d" % id_, constants.LOG_LEVEL)

    def extend_vqueue(self, vqueue):
        # If we're using model implementation, queues are located at the clients
        for rg in self.customerQueues:
            vqueue[rg].extend([req[2] for req in self.customerQueues[rg]])

    def peek_vqueue(self):
        import global_vars
        if (self.deadlineModel != "clairvoyantW" and self.deadlineModel != "clairvoyantM"):
            assert False
        vqueue = {rg: list() for rg in self.serversByReplicaGroup.iterkeys()}
        for cli in global_vars.clients:
            cli.extend_vqueue(vqueue)
        for srv in global_vars.servers:
            srv.extend_vqueue(vqueue)
        for rg in vqueue:
            vqueue[rg].sort(key=lambda x: x.deadline, reverse=False)
        return vqueue

    def peek_queue(self, rg):
        qlen = len(self.customerQueues[rg])
        if qlen > 0:
            head_deadline = self.customerQueues[rg][0][0]
        else:
            head_deadline = None
        return (qlen, head_deadline)
    
    def isDepleted(self):
        #Check if client has no requests oustanding or queued
        #print self.id, "isDepleted", self.taskRequestsArrived, self.taskResponsesReceived
        #print self.requestBatchCounter.keys()
        return (self.taskRequestsArrived - self.taskResponsesReceived) == 0

    def deactivate(self):
        pass

    def arrival(self, requestBatch):
        import global_vars
        debug = self.log.debug
        critical = self.log.critical
        partialQueues = {}
        debug("request arriving at time: %s"%Simulation.now())
        #print self.delay
        # XXX: We need to decrease the deadlines of everything that is still in
        # the queues by the delta time that has elapsed since the last time
        # we ran arrival.
        # [UPDATE] No need to do that now. We'll just set them to zero after a fixed period (as discussed with Marco)
        
        # Maps customer index into number that represents request cost
        costs = defaultdict(int)
        startTime = Simulation.now()
        maxCost = 0
        maxCostIndex = None
        self.taskRequestsArrived += 1
        self.requestBatchCounter[requestBatch[0].parentId] = 0
        self.requestCompletionTimes[requestBatch[0].parentId] = []

        vqueue = None
        if (self.deadlineModel == "clairvoyantW" or self.deadlineModel == "clairvoyantM"):
            vqueue = self.peek_vqueue()


        for req in requestBatch:
            replicaGroup = req.replicaGroup
            partialQueues.setdefault(replicaGroup, []).append(req)
            
            # TODO: Reason about different request costs
            # Note that this approach doesn't abstract subtasks as a single aggregated request
            if (self.deadlineModel == "dynamicCost"):
                wcost = req.cost * self.frg(replicaGroup, req.cost) / self.depl_rate(replicaGroup)
                costs[replicaGroup] += wcost
            else:
                costs[replicaGroup] += req.cost

            if costs[replicaGroup] > maxCost:
                maxCost = costs[replicaGroup]
                maxCostIndex = replicaGroup
            
            # Book-keeping for tracking response times later
            req.systemArrivalTime = startTime
            assert req.systemArrivalTime == req.start

        if (self.deadlineModel == "clairvoyantM"):
            maxCost = 0
            maxCostIndex = None
            for replicaGroup, rgCost in costs.iteritems():
                trgCost = rgCost
                for cReq in vqueue[replicaGroup]:
                    if (cReq.deadline <= rgCost):
                        trgCost += cReq.cost

                if trgCost > maxCost:
                    maxCost = trgCost
                    maxCostIndex = replicaGroup
        if (self.deadlineModel == "clairvoyantW"):
            maxCost = 0
            maxCostIndex = None
            for replicaGroup, rgCost in costs.iteritems():
                trgCost = rgCost
                for cReq in vqueue[replicaGroup]:
                    if (cReq.deadline <= rgCost):
                        trgCost += cReq.cost

                trgCost = trgCost/self.depl_rate(replicaGroup)
                if trgCost > maxCost:
                    maxCost = trgCost
                    maxCostIndex = replicaGroup
        if (self.deadlineModel == "newDynamicCost"):
            maxCost = 0
            maxCostIndex = None
            for replicaGroup, rgCost in costs.iteritems():
                trgCost = rgCost*self.serviceTime + self.frg(replicaGroup, rgCost)
                if trgCost > maxCost:
                    maxCost = trgCost
                    maxCostIndex = replicaGroup
        if (self.deadlineModel == "greedyDynamicCost"):
            maxCost = 0
            maxCostIndex = None
            for replicaGroup, rgCost in costs.iteritems():
                #print self.ewmas
                trgCost = rgCost*self.serviceTime + self.frg_greedy(replicaGroup, rgCost)
                if trgCost > maxCost:
                    maxCost = trgCost
                    maxCostIndex = replicaGroup
        if (self.deadlineModel == "curvefitDynamicCost"):
            maxCost = 0
            maxCostIndex = None
            for replicaGroup, rgCost in costs.iteritems():
                trgCost = rgCost*self.serviceTime + self.frg_curvefit(replicaGroup, rgCost)
                if trgCost > maxCost:
                    maxCost = trgCost
                    maxCostIndex = replicaGroup        

        # XXX: there's a problem with requests from different batches
        # coming in and interleaving with older requests. This could
        # lead to starvation or other undesirable effects.
        for customerIndex, partialQueue in partialQueues.iteritems():
            uniformIncrCost = float(maxCost) / len(partialQueue)
            uniformDeadline = Simulation.now()

            subtaskCost = sum([x.cost for x in partialQueue])
            incrSlackFrac = float(maxCost - subtaskCost) / len(partialQueue)
            incrSlack = Simulation.now()

            for req in partialQueue:
                # set BN to true if they belong to the subtask with the highest maxCost
                if(customerIndex == maxCostIndex):
                    req.BN = True
                # With uniform
                uniformDeadline += uniformIncrCost
                # With incrSlack
                incrSlack += incrSlackFrac

                if (self.deadlineModel == "constant"):
                    # NOTE: this is a naive model that serves as a baseline for comparison
                    deadline = 1
                elif (self.deadlineModel == "equalMaxCost"):
                    deadline = maxCost
                elif (self.deadlineModel == "uniformIncrCost"):
                    deadline = uniformDeadline
                elif (self.deadlineModel == "incrSlackCost"):
                    if customerIndex == maxCostIndex:
                        deadline = req.cost
                    else:
                        incrSlack += req.cost
                        deadline = incrSlack
                elif (self.deadlineModel == "dynamicCost"):
                    if customerIndex == maxCostIndex:
                        deadline = sum([r.cost for r in partialQueues[customerIndex]]) # TODO: what if req.cost?
                        self.log.debug("bottleneck request priority %d" % deadline)
                    else:
                        deadline = maxCost
                        self.log.debug("non-bottleneck request priority %d" % deadline)
                elif(self.deadlineModel == "newDynamicCost"):
                    if customerIndex == maxCostIndex:
                        deadline = costs[customerIndex] # TODO: what if req.cost?
                        req.frg = maxCost
                        self.log.debug("bottleneck request priority %d" % deadline)
                    else:
                        deadline = rgCost = costs[customerIndex]
                        overhead = self.frg_greedy(customerIndex, rgCost)
                        ridx = self.frg_ridx(customerIndex, rgCost)
                        assert (overhead+rgCost*self.serviceTime) <= maxCost
                        if(ridx < len(self.ewmas[customerIndex])-1):
                            for i in range(ridx+1, len(self.ewmas[customerIndex])):
                                overhead = self.ewmas[customerIndex][i].getMean()
                                if (costs[customerIndex]*self.serviceTime+overhead) <= maxCost:
                                    #TODO should the deadline be set to the max value in a range?
                                    deadline = self.ewma_points[i]
                                    req.frg = overhead
                elif(self.deadlineModel == "greedyDynamicCost"):
                    if customerIndex == maxCostIndex:
                        req.frg = maxCost-costs[customerIndex]
                        deadline = costs[customerIndex] # TODO: what if req.cost?
                        self.log.debug("bottleneck request priority %d" % deadline)
                        #print req.parentId, "BN", req.replicaGroup, deadline, maxCost
                    else:
                        deadline = rgCost = costs[customerIndex]
                        overhead = self.frg_greedy(customerIndex, rgCost)
                        ridx = self.frg_ridx_greedy(customerIndex, rgCost)
                        assert (overhead+rgCost*self.serviceTime) <= maxCost
                        if(ridx < len(self.ewma_points)-1):
                            for i in range(ridx+1, len(self.ewma_points)):
                                overhead = self.frg_greedy(customerIndex, i)
                                if (costs[customerIndex]*self.serviceTime+overhead) <= maxCost:
                                        deadline = self.ewma_points[i]
                                        req.frg = overhead
                        #if(ridx>=len(self.ewma_points)-1):
                        #    print req.parentId, "NBN", req.replicaGroup, deadline, overhead+costs[customerIndex], ridx, "^^"
                        #else:
                        #    print req.parentId, "NBN", req.replicaGroup, deadline, overhead+costs[customerIndex], i, self.frg_greedy(customerIndex, i+1)+costs[customerIndex]
                        
                elif(self.deadlineModel == "curvefitDynamicCost"):
                    if customerIndex == maxCostIndex:
                        req.frg = maxCost-costs[customerIndex]*self.serviceTime
                        deadline = costs[customerIndex] # TODO: what if req.cost?
                        self.log.debug("bottleneck request priority %d" % deadline)
                        #print req.parentId, customerIndex, req.frg, "B"
                    else:
                        rgCost = costs[customerIndex]
                        overhead = self.frg_curvefit(customerIndex, rgCost)
                        #ridx = self.frg_ridx(customerIndex, rgCost)
                        assert (overhead+rgCost*self.serviceTime) <= maxCost
                        # We then need to estimate what's the deadline that would be equivalent to maxCost
                        deadline = self.frg_inverse_curvefit(customerIndex, maxCost-rgCost)
                        req.frg = maxCost-rgCost
                        #print req.parentId, customerIndex, req.frg, "NB"
                elif (self.deadlineModel == "clairvoyantW"):
                    if customerIndex == maxCostIndex:
                        deadline = costs[customerIndex] # TODO: what if req.cost?
                        self.log.debug("bottleneck request priority %d" % deadline)
                    else:
                        #deadline = maxCost
                        
                        #Estimate how much we can slack but still finish with bottlenecking request
                        cost = costs[customerIndex]/self.depl_rate(customerIndex)
                        deadline = cost
                        for cReq in vqueue[customerIndex]:
                            if ((cost+cReq.cost/self.depl_rate(customerIndex)) <= maxCost):
                                cost += (cReq.cost/self.depl_rate(customerIndex))
                                deadline = cReq.deadline
                            else:
                                # I should be 1 point ahead of the requests that will bottleneck me
                                deadline = cReq.deadline - 1
                                break 
                        self.log.debug("non-bottleneck request priority %d" % deadline)
                elif (self.deadlineModel == "clairvoyantM"):
                    if customerIndex == maxCostIndex:
                        deadline = costs[customerIndex] # TODO: what if req.cost?
                        self.log.debug("bottleneck request priority %d" % deadline)
                    else:
                        deadline = maxCost
                        self.log.debug("non-bottleneck request priority %d" % deadline)

                else:
                    critical("Invalid deadline-model")
                    assert False
                
                req.deadline = deadline
                assert req.replicaGroup == customerIndex
                # TODO: transform req into an immutable object at this point
                self.enter(req)

    def enter(self, req):
        import global_vars
        debug = self.log.debug
        # NOTE: we insert each request as a tuple (deadline, arrivalTime, req) because the heap algorithm
        # performs a left-to-right compare operation to determine which item has a lower value.
        # Using the arrivalTime ensures that requests that arrive later do not jump ahead simply because
        # they reside at lower memory addresses
        heapq.heappush(self.customerQueues[req.replicaGroup], (req.deadline, req.systemArrivalTime, req))

        # DEBUG: testing with FIFO order
        # self.customerQueues[customerIndex].append( (deadline, req) )

        #print float(maxCost)*self.delay[customerIndex], float(maxCost), self.delay
        debug("client %s receiving request %s for RG %d" % (self.id, req.id, req.replicaGroup))
        global_vars.customerEnqueuingEvents[req.replicaGroup].signal()

    def updateMetricsOnRequestOut(self, req, srv):
        debug = self.log.debug
        debug("updateMetricsOnRequestOut %s %d %d" % (req.id, req.replicaGroup, req.cost))
        # Book-keeping for metrics
        self.pendingRequestsMap[srv] += 1
        self.pendingXserviceMap[srv] = \
            (1 + self.pendingRequestsMap[srv]) \
            * srv.serviceTime
        self.pendingRequestsMonitor.observe(
            "%s %s" % (srv.id,
                       self.pendingRequestsMap[srv]))

        self.workloadPerRG[req.replicaGroup] += req.cost
        # debug(self.workloadPerRG)

        req.sendTime = Simulation.now()

    def updateMetricsOnBeginProcessing(self, req, srv):
        if (not (isinstance(srv, server.CreditsServer) or isinstance(srv, server.ReplicaLeaderServer)
                 or isinstance(srv, server.C3Server))):
            # NOTE: this is because the base Server class is the theoretical model
            self.updateMetricsOnRequestOut(req, srv)

        req.serverProcessTime = Simulation.now()
        req.waitingTime = Simulation.now() - req.start
        # FIXME: only the clairvoyant strategies can get this ideal feedback
        # For other cases, the feedback is delayed and received with the response
        if (not (isinstance(srv, server.CreditsServer) or isinstance(srv, server.ReplicaLeaderServer)
                 or isinstance(srv, server.C3Server))):
            # How long the request spent waiting in the queue
            vqCost = Simulation.now() - req.systemArrivalTime 
            if (self.deadlineModel == "dynamicCost" or self.deadlineModel == "clairvoyantW" or\
                 self.deadlineModel == "clairvoyantM" or self.deadlineModel == "newDynamicCost"):
                self.update_frg(req.replicaGroup, req, vqCost)
                self.update_depl_rate(req.replicaGroup)
            if(self.deadlineModel == "greedyDynamicCost" or self.deadlineModel == "curvefitDynamicCost"):
                self.update_ewmas(req.replicaGroup, req, vqCost)

    def maybeSendShadowReads(self, replicaToServe, replicaSet):
        pass # TODO

    def onRequestComplete(self,  req, srv):
        debug = self.log.debug
        info = self.log.info
        debug("client %s receiving response %s" % (self.id, req.id))

        # OMG request completed. Time for some book-keeping
        self.reqResponsesReceived += 1
        
        self.pendingRequestsMap[srv] -= 1
        
        self.pendingXserviceMap[srv] = \
            (1 + self.pendingRequestsMap[srv]) \
            * srv.serviceTime
            
        self.pendingRequestsMonitor.observe(
            "%s %s" % (srv.id,
                       self.pendingRequestsMap[srv]))

        self.requestLatencyMonitor\
            .observe("%s %s" % (srv.id,
                     Simulation.now() - req.sendTime))

        # FIXME: Once we introduce shadow reads we know that we should not
        # track the latencies of all operations but only of the earliest
        # returning one

        #FIXME: Skip task-related book-keeping for shadow reads
        if(req.parentId == "ShadowRead"):
            return

        complTime = Simulation.now() - req.start
        self.requestCompletionTimes[req.parentId].append((req, complTime))
        self.requestBatchCounter[req.parentId] += 1
        
        #If I belong to credits/leader model, update frgs/ewmas as necessary
        if (isinstance(srv, server.ReplicaLeaderServer) or isinstance(srv, server.CreditsServer)):
            # How long the request spent waiting in the queue
            vqCost = Simulation.now() - req.sendTime + req.serverProcessTime - req.serverReceiptTime
            if (self.deadlineModel == "dynamicCost" or self.deadlineModel == "clairvoyantW" or\
                 self.deadlineModel == "clairvoyantM" or self.deadlineModel == "newDynamicCost"):
                self.update_frg(req.replicaGroup, req, vqCost)
                self.update_depl_rate(req.replicaGroup)
            if(self.deadlineModel == "greedyDynamicCost" or self.deadlineModel == "curvefitDynamicCost"):
                self.update_ewmas(req.replicaGroup, req, vqCost)

        #info("req %s completes with response time %f" % (req.id, complTime))
        if(self.requestBatchCounter[req.parentId] == req.batchsize):
            #info("task %s completes with response time %f" % (req.parentId, complTime))
            self.taskResponsesReceived += 1
            rgs = [x[0].replicaGroup for x in self.requestCompletionTimes[req.parentId]]
            subtaskCompletionTimes = {rg: 0 for rg in rgs}
            for r, ct in self.requestCompletionTimes[req.parentId]:
                if ct>subtaskCompletionTimes[r.replicaGroup]:
                    subtaskCompletionTimes[r.replicaGroup] = ct
            rgBottleneck = max(subtaskCompletionTimes.iteritems(), key=operator.itemgetter(1))[0]
            for tup in self.requestCompletionTimes[req.parentId]:
                #print tup[0].replicaGroup, rgBottleneck, tup[0].BN
                if tup[0].replicaGroup == rgBottleneck and not tup[0].BN:
                    self.falseBottleneckEstCount += 1
                    break
            rtMean = numpy.mean(subtaskCompletionTimes.values())
            rtSD = numpy.std(subtaskCompletionTimes.values())
            rtrSD = rtSD/rtMean*100 #Calculate the relative SD
            self.intraBatchRTRSDMonitor.observe(rtrSD)
            del self.requestCompletionTimes[req.parentId]
            del self.requestBatchCounter[req.parentId]
            self.latencyMonitor.observe("%s %s" % (complTime, self.id))

    def frg_ridx(self, rg, cost):       
        ridx = 0
        for range_right in self.ewma_points:
            if cost <= range_right:
                break
            ridx += 1
        ridx = min(ridx, len(self.ewma_points) -1)
        self.log.debug("frg_ridx(%d, %d) -> %d" % (rg, cost, ridx))
        return ridx
            
    def frg(self, rg, cost):
        ridx = self.frg_ridx(rg, cost)
        return self.ewmas[rg][ridx].getMean()

    def update_frg(self, rg, req, vqCost):
        # NOTE: ensure that rate is always > 0 or it will cause a divide by zero error
        
        #Previous FRG Bin-based Calculation
        ridx = self.frg_ridx(rg, req.deadline)
        # TODO: try using harmonic mean instead of EWMA
        e = self.ewmas[rg][ridx]
        e.update(vqCost)
        self.frgMonitor.observe("%s %s %s"%(rg, self.ewma_points[ridx], e.getMean()))
        self.frgVarMonitor.observe(e.getVolatility())

    #-------Greedy Dynamic Cost Functions------
    def frg_ridx_greedy(self, rg, deadline):
        #print deadline, self.ewmas[rg].keys()
        minRidx = 0
        minValue = float("inf")
        for x in self.ewmas[rg].keys():
            if(minValue > abs(self.ewma_points[x]-deadline)):
                minValue = abs(self.ewma_points[x]-deadline)
                minRidx = x
        #n = min(self.ewmas[rg].keys(), key=lambda x:abs(self.ewma_points[x]-deadline))
        #ridx = self.ewma_points.index(n)
        return minRidx

    def frg_greedy(self, rg, deadline):
        ridx = self.frg_ridx_greedy(rg, deadline)
        return self.frg_greedy_map[rg][ridx]

    def frg_calculate_greedy(self, rg, deadline):
        th = sum([e.n for e in self.ewmas[rg].values()])
        th = math.ceil(float(th)/len(self.ewmas[rg].keys()))
        ridx = self.frg_ridx_greedy(rg, deadline)
        if(self.ewmas[rg][ridx].isStale()):
            self.ewmas[rg][ridx].wipe()
        cum_sample = self.ewmas[rg][ridx].n
        frg = self.ewmas[rg][ridx].getMean() * cum_sample
        nRange = max(ridx, len(self.ewma_points)-ridx)
        for i in range(nRange):
            x = ridx + i
            y = ridx - i
            if(x<len(self.ewma_points)):
                cum_sample += self.ewmas[rg][x].n
                frg += (self.ewmas[rg][x].getMean() * self.ewmas[rg][x].n)
            if(y>=0):
                cum_sample += self.ewmas[rg][y].n
                frg += (self.ewmas[rg][y].getMean() * self.ewmas[rg][y].n)  
            if(cum_sample>=th):
                break
        if(cum_sample == 0):
            frg = 0
        else:
            frg = frg/cum_sample
        return frg
                
    def update_frg_greedy(self):
        nonMono = 0
        total = 0
        for rg in self.ewmas.keys():
            for i in self.ewmas[rg].keys():
                e = self.ewmas[rg][i]
                if e.isStale():
                    e.wipe()
        for rg in self.ewmas.keys():
            for i in self.ewmas[rg].keys():
                n = self.ewma_points[i]
                frg = self.frg_calculate_greedy(rg, n)
                #Handle lack of monotonicity
                if(i>0 and frg<self.frg_greedy_map[rg][i-1]):
                    nonMono += 1
                    frg = self.frg_greedy_map[rg][i-1]
                if e.isStale():
                    e.wipe()
                    e.update(frg)
                self.frg_greedy_map[rg][i] = frg
                total += 1
                self.frgMonitor.observe("%s %s %s"%(rg, self.ewma_points[i], frg))
        self.greedyNonMonotonicityMonitor.observe(nonMono/total*100.0)
                   
    #-------Curve-fitting Dynamic Cost Functions------
    def frg_curvefit(self, rg, deadline):
        y = self.polyfunc[rg](deadline)
        return y
    
    def frg_inverse_curvefit(self, rg, maxCost):
        f = self.polyfunc[rg]
        roots = (f - maxCost).roots
        if len(roots) == 0:
            #This means we don't have sufficient knowledge about queueing delays
            #set x to maxCost (TODO: Reason about this choice!)
            return maxCost
        for x in roots:
        #We take the positive root
            if(x>0):
                return x
        assert False
              
    def frg_curvefit_update(self):
        #Calculate polynomial function for every RG
        for rg in self.ewmas.keys():
            ewmaList = []
            for n in self.ewmas[rg].keys():
                if(self.ewmas[rg][n].isStale()):
                    #Set n to 0 and wipe out value (this is to prevent this from affecting curve fitting)
                    self.ewmas[rg][n].wipe()
                    pass
                else:
                    if(self.ewmas[rg][n].n>0):    
                        ewmaList.extend([(self.ewma_points[n], self.ewmas[rg][n].getMean())]*self.ewmas[rg][n].n)
            #If we have no EWMAs, do nothing
            if(len(ewmaList)==0):
                return
            points = numpy.array(ewmaList)
            # get x and y vectors
            x = points[:,0]
            y = points[:,1]
            
            # calculate polynomial
            # TODO Revisit the curve-fitting approach and test out alternatives
            z = numpy.polyfit(x, y, 3)
            f = numpy.poly1d(z)
            self.polyfunc[rg] = f

    def update_ewmas(self, rg, req, vqCost):
        #Updates the EWMAs used for all dynamic approaches
        ridx = self.frg_ridx_greedy(req.replicaGroup, req.deadline)
        self.ewmas[req.replicaGroup][ridx].update(vqCost)
        #self.frgMonitor.observe("%s %s %s"%(rg, self.ewma_points[ridx], self.ewmas[req.replicaGroup][ridx].getMean()))

    def depl_rate(self, rg):
        return self.depl_rate_map[rg]

    def update_depl_rate(self, rg):
        alpha = 0.9
        delta = Simulation.now() - self.last_update_depl_rate_time
        self.last_update_depl_rate_time = Simulation.now()

        if delta == 0:
            return
        new_rate = 1 / delta
        self.depl_rate_map[rg] = self.depl_rate_map[rg]*(1.0-alpha) + new_rate*alpha
        # NOTE: ensure that rate is always > 0 or it will cause a divide by zero error
        assert self.depl_rate_map[rg] > 0

    def sendRequest(self, req, srv):
        debug = self.log.debug
        debug("sending req %s to %s @:%f" % (req, srv, Simulation.now()))
        self.updateMetricsOnRequestOut(req, srv)

        delay = constants.NW_LATENCY_BASE + \
            random.normalvariate(constants.NW_LATENCY_MU,
                                 constants.NW_LATENCY_SIGMA)
        deliver = DeliverRequestWithDelay()
        Simulation.activate(deliver,
                            deliver.run(req, delay, srv),
                            at=Simulation.now())

    def rank(self, replicas):
        # Do the replica ranking by least outstanding requests
        return sorted(replicas, cmp=lambda x,y: cmp(self.pendingRequestsMap[x], self.pendingRequestsMap[y]))

        # TODO: should we normalize cost * \mu instead?
        # min_qlen = float("inf")
        # replicaToServe = None
        # # NOTE: use reverse order so that in case of tie we select the group "leader"
        # for srv in reversed(global_vars.serversByReplicaGroup[req.replicaGroup]):
        #     if len(self.backlogQueues[srv]) < min_qlen:
        #         min_qlen = len(self.backlogQueues[srv])
        #         replicaToServe = srv

class C3Client(Client):
    def __init__(self, id_, latencyMonitor, serverList, serversByReplicaGroup, replicationFactor,
                 shadowReadRatio, demandWeight, deadlineModel, serviceTime, cubicC, cubicSmax,
                 cubicBeta, rateInterval, concurrencyWeight, hysterisisFactor, selectionStrategy,
                 backpressure, costExponent):
        Client.__init__(self, id_, latencyMonitor, serverList, serversByReplicaGroup, replicationFactor,
                 shadowReadRatio, demandWeight, deadlineModel, serviceTime, cubicC, cubicSmax,
                 cubicBeta, rateInterval, concurrencyWeight, hysterisisFactor, selectionStrategy,
                 backpressure, costExponent)

        # Last-received response time of server
        self.responseTimesMap = {node: 0 for node in serverList}

        # Record waiting and service times as relayed by the server
        self.expectedDelayMap = {node: {} for node in serverList}
        self.lastSeen = {node: 0 for node in serverList}

        # Round robin parameters
        self.rrIndex = {node: 0 for node in serverList}

        # Rate limiters per replica
        self.rateLimiters = {node: ratelimiter.RateLimiter("RL-%s" % node.id,
                                               self,
                                               rate=rateInterval / node.serviceTime * node.resourceCapacity,
                                               rateInterval=rateInterval) for node in serverList}
        self.lastRateDecrease = {node: 0 for node in serverList}
        self.valueOfLastDecrease = {node: 10 for node in serverList}
        self.receiveRate = {node: ReceiveRate("RL-%s" % node.id, rateInterval)
                            for node in serverList}
        self.lastRateIncrease = {node: 0 for node in serverList}
        self.rateInterval = rateInterval

        self.rateMonitor = Simulation.Monitor(name="AlphaMonitor")
        self.receiveRateMonitor = Simulation.Monitor(name="ReceiveRateMonitor")
        
        # Backpressure related initialization
        if (backpressure is True):
            self.backpressureSchedulers = \
                {node: BackpressureScheduler("BP-%s" % node.id, self)
                 for node in serverList}
            for node in serverList:
                Simulation.activate(self.backpressureSchedulers[node],
                                    self.backpressureSchedulers[node].run(),
                                    at=Simulation.now())

        # ds-metrics
        if (selectionStrategy == "ds"):
            self.latencyEdma = {node: ExponentiallyDecayingSample(100,
                                                                  0.75,
                                                                  self.clock)
                                for node in serverList}
            self.dsScores = {node: 0 for node in serverList}
            for node, rateLimiter in self.rateLimiters.items():
                ds = DynamicSnitch(self, 100)
                Simulation.activate(ds, ds.run(),
                                    at=Simulation.now())
                        
        self.customerQueues = None

    def init_logger(self, id_):
        return logger.getLogger("C3Client%d" % id_, constants.LOG_LEVEL)

    def extend_vqueue(self, vqueue):
        #TODO
        pass

    def peek_queue(self, rg):
        #TODO
        return (0, 0)

    def deactivate(self):
        for node in self.receiveRate:
            self.receiveRate[node].active = False

    def enter(self, req):
        replicaSet = self.serversByReplicaGroup[req.replicaGroup]
        sortedReplicaSet = self.sort(replicaSet)
        replicaToServe = sortedReplicaSet[0]
        if(self.backpressure is False):
            self.sendRequest(req, replicaToServe)
            self.maybeSendShadowReads(replicaToServe, replicaSet, req.replicaGroup, req.cost, req.deadline)
        else:
            self.backpressureSchedulers[replicaToServe].enqueue(req,
                                                               replicaSet)

    def sort(self, originalReplicaSet):

        replicaSet = originalReplicaSet[0:]

        if(self.REPLICA_SELECTION_STRATEGY == "RAND"):
            # Pick a random node for the request.
            # Represents SimpleSnitch + uniform request access.
            # Ignore scores and everything else.
            random.shuffle(replicaSet)
        elif(self.REPLICA_SELECTION_STRATEGY == "RR"):
            # Round-robin
            index = self.rrIndex[replicaSet[0]]
            replicaSetNew = replicaSet[index:] + replicaSet[:index]
            self.rrIndex[replicaSet[0]] = (index + 1) % len(replicaSet)
            replicaSet = replicaSetNew
        elif(self.REPLICA_SELECTION_STRATEGY == "LOR"):
            # Least Outstanding Requests
            replicaSet.sort(key=self.pendingRequestsMap.get)
        elif(self.REPLICA_SELECTION_STRATEGY == "LRT"):
            # Least Response Time
            replicaSet.sort(key=self.responseTimesMap.get)
        elif(self.REPLICA_SELECTION_STRATEGY == "WRRT"):
            # Weighted random proportional to response times
            m = {}
            for each in replicaSet:
                if ("serviceTime" not in self.expectedDelayMap[each]):
                    m[each] = 0.0
                else:
                    m[each] = self.expectedDelayMap[each]["serviceTime"]
            replicaSet.sort(key=m.get)
            total = sum(map(lambda x: self.responseTimesMap[x], replicaSet))
            selection = random.uniform(0, total)
            cumSum = 0
            nodeToSelect = None
            i = 0
            if (total != 0):
                for entry in replicaSet:
                    cumSum += self.responseTimesMap[entry]
                    if (selection < cumSum):
                        nodeToSelect = entry
                        break
                    i += 1
                assert nodeToSelect is not None

                replicaSet[0], replicaSet[i] = replicaSet[i], replicaSet[0]
        elif(self.REPLICA_SELECTION_STRATEGY == "PRIMARY"):
            pass
        elif(self.REPLICA_SELECTION_STRATEGY == "pendingXserviceTime"):
            # Sort by response times * client-local-pending-requests
            replicaSet.sort(key=self.pendingXserviceMap.get)
        elif(self.REPLICA_SELECTION_STRATEGY == "ORA"):
            # Sort by response times * pending-requests
            oracleMap = {replica: (1 + len(replica.queueResource.activeQ
                                   + replica.queueResource.waitQ))
                         * replica.serviceTime
                         for replica in originalReplicaSet}
            replicaSet.sort(key=oracleMap.get)
        elif(self.REPLICA_SELECTION_STRATEGY == "C3"):
            # Queue-size-estimate ^ b times service time product
            sortMap = {}
            for replica in originalReplicaSet:
                sortMap[replica] = self.computeExpectedDelay(replica)
            replicaSet.sort(key=sortMap.get)
        elif(self.REPLICA_SELECTION_STRATEGY == "2C"):
            # Uses the Queue-size-estimate ^ b and service time product, but
            # unlike the C3 mechanism, desynchronizes using a two
            # choices selection instead of the outstanding requests.
            sortMap = {}
            random.shuffle(replicaSet)
            twoChoices, rest = replicaSet[:2], replicaSet[2:]
            for replica in twoChoices:
                sortMap[replica] = self.computeMetricTwoChoices(replica)
            for replica in rest:
                sortMap[replica] = float("inf")
            replicaSet.sort(key=sortMap.get)
        elif(self.REPLICA_SELECTION_STRATEGY == "DS"):
            # Dynamic snitching mechanism, albeit
            # without the gossiped iowaits
            firstNode = replicaSet[0]
            firstNodeScore = self.dsScores[firstNode]
            badnessThreshold = 0.0

            if (firstNodeScore != 0.0):
                for node in replicaSet[1:]:
                    newNodeScore = self.dsScores[node]
                    if ((firstNodeScore - newNodeScore)/firstNodeScore
                       > badnessThreshold):
                        replicaSet.sort(key=self.dsScores.get)
        else:
            print self.REPLICA_SELECTION_STRATEGY
            assert False, "REPLICA_SELECTION_STRATEGY isn't set or is invalid"

        return replicaSet

    def metricDecay(self, replica):
        return math.exp(-(Simulation.now() - self.lastSeen[replica])
                        / (2 * self.rateInterval))

    def computeExpectedDelay(self, replica):
        total = 0
        if (len(self.expectedDelayMap[replica]) != 0):
            metricMap = self.expectedDelayMap[replica]
            twiceNetworkLatency = metricMap["nw"]
            theta = (1 + self.pendingRequestsMap[replica]
                     * self.concurrencyWeight
                     + metricMap["queueSizeAfter"])
            total += (twiceNetworkLatency +
                      ((theta ** self.costExponent)
                       * (metricMap["serviceTime"])))
        else:
            return 0
        return total

    def computeMetricTwoChoices(self, replica):
        total = 0
        if (len(self.expectedDelayMap[replica]) != 0):
            metricMap = self.expectedDelayMap[replica]
            twiceNetworkLatency = metricMap["nw"]
            theta = (1 + metricMap["queueSizeAfter"])
            total += (twiceNetworkLatency +
                      ((theta ** self.costExponent)
                       * (metricMap["serviceTime"])))
        else:
            return 0
        return total

    def maybeSendShadowReads(self, replicaToServe, replicaSet, replicaGroup, cost, deadline):
        if (random.uniform(0, 1.0) < self.shadowReadRatio):
            for replica in replicaSet:
                if (replica is not replicaToServe):
                    #FIXME need to determine cost of shadowreads (currently setting it to 1)
                    shadowReadTask = request.Request(fullId="ShadowRead",
                                        parentId="ShadowRead",
                                        key=1,
                                        replicaGroup=replicaGroup,
                                        batchsize=1,
                                        sendingClient=self,
                                        cost=cost)
                    shadowReadTask.deadline = deadline #FIXME: should we give precedence to shadow reads?
                    shadowReadTask.systemArrivalTime = Simulation.now()
                    self.sendRequest(shadowReadTask, replica)
                    self.rateLimiters[replica].acquire(shadowReadTask.cost)

    def updateEma(self, replica, metricMap):
        alpha = 0.9
        if (len(self.expectedDelayMap[replica]) == 0):
            self.expectedDelayMap[replica] = metricMap
            return

        for metric in metricMap:
            self.expectedDelayMap[replica][metric] \
                = alpha * metricMap[metric] + (1 - alpha) \
                * self.expectedDelayMap[replica][metric]

    def updateRates(self, replica, metricMap, task):
        # Cubic Parameters go here
        # beta = 0.2
        # C = 0.000004
        # Smax = 10
        beta = self.cubicBeta
        C = self.cubicC
        Smax = self.cubicSmax
        hysterisisFactor = self.hysterisisFactor
        currentSendingRate = self.rateLimiters[replica].rate
        currentReceiveRate = self.receiveRate[replica].getRate()

        if (currentSendingRate < currentReceiveRate):
            # This means that we need to bump up our own rate.
            # For this, increase the rate according to a cubic
            # window. Rmax is the sending-rate at which we last
            # observed a congestion event. We grow aggressively
            # towards this point, and then slow down, stabilise,
            # and then advance further up. Every rate
            # increase is capped by Smax.
            T = Simulation.now() - self.lastRateDecrease[replica]
            self.lastRateIncrease[replica] = Simulation.now()
            Rmax = self.valueOfLastDecrease[replica]

            newSendingRate = C * (T - (Rmax * beta/C)**(1.0/3.0))**3 + Rmax

            if (newSendingRate - currentSendingRate > Smax):
                self.rateLimiters[replica].rate += Smax
            else:
                self.rateLimiters[replica].rate = newSendingRate
        elif (currentReceiveRate == 0):
            pass
        elif (currentSendingRate > currentReceiveRate
              and Simulation.now() - self.lastRateIncrease[replica]
              > self.rateInterval * hysterisisFactor):
            # The hysterisis factor in the condition is to ensure
            # that the receive-rate measurements have enough time
            # to adapt to the updated rate.

            # So we're in here now, which means we need to back down.
            # Multiplicatively decrease the rate by a factor of beta.
            self.valueOfLastDecrease[replica] = currentSendingRate
            self.rateLimiters[replica].rate *= beta
            self.rateLimiters[replica].rate = \
                max(self.rateLimiters[replica].rate, 0.01)
            self.lastRateDecrease[replica] = Simulation.now()

        assert (self.rateLimiters[replica].rate > 0)
        alphaObservation = (replica.id,
                            self.rateLimiters[replica].rate)
        receiveRateObs = (replica.id,
                          self.receiveRate[replica].getRate())
        self.rateMonitor.observe(alphaObservation[1])
        self.receiveRateMonitor.observe(receiveRateObs[1])
        
    def onRequestComplete(self,  req, srv):
        Client.onRequestComplete(self, req, srv)
        
        debug = self.log.debug
        info = self.log.info
        
        self.responseTimesMap[srv] = \
                Simulation.now() - req.sendTime
                
        # OMG request completed. Time for some C3-specific book-keeping      
        metricMap = req.feedback
        metricMap["responseTime"] = self.responseTimesMap[srv]
        metricMap["nw"] = metricMap["responseTime"] - metricMap["serviceTime"]
        self.updateEma(srv, metricMap)
        self.receiveRate[srv].add(req.cost)

        # Backpressure related book-keeping
        if (self.backpressure):
            self.updateRates(srv, metricMap, req)

        self.lastSeen[srv] = Simulation.now()

        if (self.REPLICA_SELECTION_STRATEGY == "ds"):
            self.latencyEdma[srv]\
                  .update(metricMap["responseTime"])
     
    class ResponseHandler(Simulation.Process):
        def __init__(self):
            Simulation.Process.__init__(self, name='ResponseHandler')
    
        def run(self, client, task, replicaThatServed):
            yield Simulation.hold, self,
            yield Simulation.waitevent, self, task.completionEvent
    
            delay = constants.NW_LATENCY_BASE + \
                random.normalvariate(constants.NW_LATENCY_MU,
                                     constants.NW_LATENCY_SIGMA)
            yield Simulation.hold, self, delay
    
            # OMG request completed. Time for some book-keeping
            client.pendingRequestsMap[replicaThatServed] -= 1
            client.pendingXserviceMap[replicaThatServed] = \
                (1 + client.pendingRequestsMap[replicaThatServed]) \
                * replicaThatServed.serviceTime
    
            client.pendingRequestsMonitor.observe(
                "%s %s" % (replicaThatServed.id,
                           client.pendingRequestsMap[replicaThatServed]))
    
            client.responseTimesMap[replicaThatServed] = \
                Simulation.now() - client.taskSentTimeTracker[task]
            client.latencyTrackerMonitor\
                  .observe("%s %s" % (replicaThatServed.id,
                           Simulation.now() - client.taskSentTimeTracker[task]))
            metricMap = task.completionEvent.signalparam
            metricMap["responseTime"] = client.responseTimesMap[replicaThatServed]
            metricMap["nw"] = metricMap["responseTime"] - metricMap["serviceTime"]
            client.updateEma(replicaThatServed, metricMap)
            client.receiveRate[replicaThatServed].add(1)
    
            # Backpressure related book-keeping
            if (client.backpressure):
                client.updateRates(replicaThatServed, metricMap, task)
    
            client.lastSeen[replicaThatServed] = Simulation.now()
    
            if (client.REPLICA_SELECTION_STRATEGY == "ds"):
                client.latencyEdma[replicaThatServed]\
                      .update(metricMap["responseTime"])
    
            del client.taskSentTimeTracker[task]
            del client.taskArrivalTimeTracker[task]
    
            # Does not make sense to record shadow read latencies
            # as a latency measurement
            if (task.latencyMonitor is not None):
                client.taskBatchCounter[task.parentId] += 1
                if(client.taskBatchCounter[task.parentId] == task.batchsize):
                    del client.taskBatchCounter[task.parentId]
                    task.latencyMonitor.observe("%s %s" %
                                                (Simulation.now() - task.start,
                                                 client.id))


class BackpressureScheduler(Simulation.Process):
    def __init__(self, id_, client):
        self.id = id_
        self.backlogQueue = []
        self.client = client
        self.count = 0
        self.backlogReadyEvent = Simulation.SimEvent("BacklogReady")
        Simulation.Process.__init__(self, name='BackpressureScheduler')

    def run(self):
        while(1):
            yield Simulation.hold, self,

            if (len(self.backlogQueue) != 0):
                task, replicaSet = self.backlogQueue[0]
                sortedReplicaSet = self.client.sort(replicaSet)
                sent = False
                minDurationToWait = 1e10   # arbitrary large value
                minReplica = None
                for replica in sortedReplicaSet:
                    currentTokens = self.client.rateLimiters[replica].tokens
#                    self.client.tokenMonitor.observe("%s %s"
#                                                     % (replica.id,
#                                                        currentTokens))
                    isAcquired, durationToWait, futureTokens = \
                        self.client.rateLimiters[replica].tryAcquire(task.cost)
                    if (isAcquired):
                        assert round(self.client.
                                     rateLimiters[replica].tokens, 9) >= round(task.cost, 9)
                        self.backlogQueue.pop(0)
#                        if(task.parentId == '0-Request9710'):
#                            print ">>>> Sending request 9710", "total size:", task.batchsize
                        self.client.sendRequest(task, replica)
                        self.client.maybeSendShadowReads(replica, replicaSet, task.replicaGroup, task.cost, task.deadline)
                        sent = True
                        self.client.rateLimiters[replica].acquire(task.cost)
                        break
                    else:
                        if durationToWait < minDurationToWait:
                            minDurationToWait = durationToWait
                            minReplica = replica
                        assert self.client.rateLimiters[replica].tokens < task.cost

                if (not sent):
#                    if(task.parentId == '0-Request9710'):
#                        print ">>>> Waiting for request 9710", minDurationToWait, "ms, total size:", task.batchsize
#                        print minReplica, self.client.rateLimiters[minReplica].tokens, self.client.rateLimiters[minReplica].rate, self.client.rateLimiters[minReplica].lastSent
                    # Backpressure mode. Wait for the least amount of time
                    # necessary until at least one rate limiter is expected
                    # to be available
                    yield Simulation.hold, self, minDurationToWait
                    # NOTE: In principle, these 2 lines below would not be
                    # necessary because the rate limiter would have exactly 1
                    # token after minDurationWait. However, due to
                    # floating-point arithmetic precision we might not have 1
                    # token and this would cause the simulation to enter an
                    # almost infinite loop. These 2 lines by-pass this problem.
                    self.client.rateLimiters[minReplica].update(task.cost)
                    minReplica = None
            else:
                yield Simulation.waitevent, self, self.backlogReadyEvent
                self.backlogReadyEvent = Simulation.SimEvent("BacklogReady")

    def enqueue(self, task, replicaSet):
        self.backlogQueue.append((task, replicaSet))
        self.backlogReadyEvent.signal()


class ReceiveRate(Simulation.Process):
    def __init__(self, id, interval):
        self.rate = 0
        self.id = id
        self.active = True
        self.interval = interval
        self.count = 0
        Simulation.Process.__init__(self, name=self.id)
        Simulation.activate(self, self.run(), at=Simulation.now())

    def getRate(self):
        return self.rate

    def run(self):
        while(self.active):
            yield Simulation.hold, self, self.interval
            alpha = 0.1
            self.rate = alpha * self.count + (1 - alpha) * self.rate
            self.count = 0

    def add(self, requests):
        self.count += requests


class DynamicSnitch(Simulation.Process):
    '''
    Model for Cassandra's native dynamic snitching approach
    '''
    def __init__(self, client, snitchUpdateInterval):
        self.SNITCHING_INTERVAL = snitchUpdateInterval
        self.client = client
        Simulation.Process.__init__(self, name='DynamicSnitch')

    def run(self):
        # Start each process with a minor delay

        while(1):
            yield Simulation.hold, self, self.SNITCHING_INTERVAL

            # Adaptation of DynamicEndpointSnitch algorithm
            maxLatency = 1.0
            maxPenalty = 1.0
            latencies = [entry.get_snapshot().get_median()
                         for entry in self.client.latencyEdma.values()]
            latenciesGtOne = [latency for latency
                              in latencies if latency > 1.0]
            if (len(latencies) == 0):  # nothing to see here
                continue
            maxLatency = max(latenciesGtOne) if len(latenciesGtOne) > 0 \
                else 1.0
            penalties = {}
            for peer in self.client.serverList:
                penalties[peer] = self.client.lastSeen[peer]
                penalties[peer] = Simulation.now() - penalties[peer]
                if (penalties[peer] > self.SNITCHING_INTERVAL):
                    penalties[peer] = self.SNITCHING_INTERVAL

            penaltiesGtOne = [penalty for penalty in penalties.values()
                              if penalty > 1.0]
            maxPenalty = max(penalties.values()) \
                if len(penaltiesGtOne) > 0 else 1.0

            for peer in self.client.latencyEdma:
                score = self.client.latencyEdma[peer] \
                            .get_snapshot() \
                            .get_median() / float(maxLatency)

                if (peer in penalties):
                    score += penalties[peer] / float(maxPenalty)
                else:
                    score += 1
                assert score >= 0 and score <= 2.0
                self.client.dsScores[peer] = score

class ReplicaLeaderClient(Client):
    def __init__(self, id_, latencyMonitor, serverList, serversByReplicaGroup, replicationFactor,
                 shadowReadRatio, demandWeight, deadlineModel, serviceTime, cubicC, cubicSmax,
                 cubicBeta, rateInterval, concurrencyWeight, hysterisisFactor, selectionStrategy,
                 backpressure, costExponent):
        Client.__init__(self, id_, latencyMonitor, serverList, serversByReplicaGroup, replicationFactor,
                 shadowReadRatio, demandWeight, deadlineModel, serviceTime, cubicC, cubicSmax,
                 cubicBeta, rateInterval, concurrencyWeight, hysterisisFactor, selectionStrategy,
                 backpressure, costExponent)

        self.customerQueues = None

    def init_logger(self, id_):
        return logger.getLogger("ReplicaLeaderClient%d" % id_, constants.LOG_LEVEL)

    def extend_vqueue(self, vqueue):
        pass

    def peek_queue(self, rg):
        assert False
        pass

    def enter(self, req):
        import global_vars
        debug = self.log.debug
        leader = global_vars.serversByReplicaGroup[req.replicaGroup][0]
        debug("client %s sending request %s for RG %d" % (self.id, req.id, req.replicaGroup))
        self.sendRequest(req, leader)

class CreditsClient(Client):
    def __init__(self, id_, latencyMonitor, serverList, serversByReplicaGroup, replicationFactor,
                 shadowReadRatio, demandWeight, deadlineModel, serviceTime, cubicC, cubicSmax,
                 cubicBeta, rateInterval, concurrencyWeight, hysterisisFactor, selectionStrategy,
                 backpressure, costExponent):
        Client.__init__(self, id_, latencyMonitor, serverList, serversByReplicaGroup, replicationFactor,
                 shadowReadRatio, demandWeight, deadlineModel, serviceTime, cubicC, cubicSmax,
                 cubicBeta, rateInterval, concurrencyWeight, hysterisisFactor, selectionStrategy,
                 backpressure, costExponent)

        rateInterval = constants.CTRL_INTERVAL
        self.rateLimiters = {srv: ratelimiter.RateLimiter('RateLimiter-%s-%s' % (id_, srv.id),
                                                          self,
                                                          rate=rateInterval / srv.serviceTime * srv.resourceCapacity,
                                                          rateInterval=rateInterval) for srv in serverList}

        self.executors = {rg: CreditsClientExecutor(rg, self) for rg in serversByReplicaGroup.iterkeys()}
        self.reporter = CreditsClientReporter(self)

        self.customerQueues = None

    def init_logger(self, id_):
        return logger.getLogger("CreditsClient%d" % id_, constants.LOG_LEVEL)

    def extend_vqueue(self, vqueue):
        import global_vars
        # If we're using credits implementation, queues are located at the executors and at the servers
        for rg, e in self.executors.iteritems():
            vqueue[rg].extend([req[2] for req in self.executors[rg].backlogQueue])

    def peek_queue(self, rg):
        qlen = len(self.executors[rg].backlogQueue)
        if qlen > 0:
            head_deadline = self.executors[rg].backlogQueue[0][0]
        else:
            head_deadline = None
        return (qlen, head_deadline)

    def updateCredits(self, creds):
        for srv, tokens in creds.iteritems():
            self.rateLimiters[srv].update(tokens)

    def enter(self, req):
        debug = self.log.debug
        debug("client %s enqueuing request %s for RG %d" % (self.id, req.id, req.replicaGroup))
        self.executors[req.replicaGroup].enqueue(req)

class CreditsClientReporter(Simulation.Process):

    def __init__(self, client):
        self.client = client
        Simulation.Process.__init__(self,
                                    name='CreditsClientReporter-%s' % (client.id))
        Simulation.activate(self, self.run(), Simulation.now())

    def run(self):
        import global_vars
        rgs = global_vars.serversByReplicaGroup.keys()

        interval = constants.CTRL_INTERVAL

        debug = self.client.log.debug
        warning = self.client.log.warning
        critical = self.client.log.critical

        while (workload.finished() == False):
            yield Simulation.hold, self, interval
            # debug("reporting demand feedback")

            utilizations = defaultdict(int)
            backlogs = defaultdict(int)
            outstandings = defaultdict(int)

            for rg in rgs:
                utilizations[rg] = self.client.workloadPerRG[rg]
                self.client.workloadPerRG[rg] = 0
                backlogs[rg] = sum(map(lambda x: x[2].cost, self.client.executors[rg].backlogQueue))

            # debug(backlogs)

            # NOTE: we do this instead of a copy because we use a defaultdict so that servers without
            # pending requests are always zero
            for srv in self.client.serverList:
                outstandings[srv] = self.client.pendingRequestsMap[srv]

            # DEBUG
            # global_vars.controller.enqueueDemandFeedback(self.client, utilizations, outstandings, backlogs)

            delay = constants.NW_LATENCY_BASE + \
                random.normalvariate(constants.NW_LATENCY_MU,
                                     constants.NW_LATENCY_SIGMA)
            feedback = controller.DeliverDemandFeedbackWithDelay()
            Simulation.activate(feedback,
                                feedback.run(self.client, utilizations, backlogs, outstandings,
                                             delay, global_vars.controller),
                                at=Simulation.now())


class CreditsClientExecutor(Simulation.Process):

    def __init__(self, id_, client):
        self.id = id_
        self.client = client
        self.backlogQueue = list()
        self.enqueueingEvent = Simulation.SimEvent("Enqueuing-Event-%s-%s" % (client.id, id_))

        Simulation.Process.__init__(self,
                                    name='CreditsClientExecutor-%s-%s' % (client.id, id_))
        Simulation.activate(self, self.run(), Simulation.now())

    def enqueue(self, req):
        heapq.heappush(self.backlogQueue, (req.deadline, req.systemArrivalTime, req))
        self.enqueueingEvent.signal()

    def run(self):
        import global_vars
        debug = self.client.log.debug
        info = self.client.log.info
        warning = self.client.log.warning
        critical = self.client.log.critical
        while (True):

            # Check whether any request is already waiting to be processed
            while (True):
                # Wait for a notification when a request arrives at this server
                # debug("going to yield")
                event = self.enqueueingEvent
                yield Simulation.queueevent, self, [event]
                # assert event.occurred == False

                # debug("checking tot enqueued requests")
                tot_qlen = len(self.backlogQueue)
                # debug("tot enqueued requests %d" % tot_qlen)

                if tot_qlen > 0:
                    debug("tot enqueued requests %d" % tot_qlen)
                    break

            # Send all requests within the cred limits
            minDurationToWait = float("inf")
            minSrv = None
            futureTokens = None

            sent = set()
            for i in range(len(self.backlogQueue)):
                deadline, arrivalTime, req = self.backlogQueue[i]
                replicas = global_vars.serversByReplicaGroup[req.replicaGroup]
                # debug(replicas)
                rankedServers = self.client.rank(replicas)
                # debug(rankedServers)

                for srv in rankedServers:
                    # FIXME: implement monitor
                    # currentTokens = self.client.rateLimiters[srv].getTokens(Simulation.now())
                    # self.client.tokenMonitor.observe("%s %s" % (srv.id, currentTokens))
                    statusOk, durationToWait, futureTokens = self.client.rateLimiters[srv].tryAcquire(req.cost)
                    if statusOk:
                        assert round(self.client.rateLimiters[srv].tokens, 9) >= round(req.cost, 9)
                        sent.add(i)
                        self.client.sendRequest(req, srv)
                        self.client.rateLimiters[srv].acquire(req.cost)
                        break
                    else:
                        debug("cannot send req %s of cost %f to %s because rate limiter (%s) reports to wait future tokens %f in %f" %
                              (req,req.cost,srv,self.client.rateLimiters[srv],futureTokens,durationToWait))
                        if durationToWait < minDurationToWait:
                            minDurationToWait = durationToWait
                            minSrv = srv
                        assert self.client.rateLimiters[srv].tokens < req.cost

            # Remove sent requests from backlogQueue and re-heapify the queue
            # info(sent)
            # info(self.backlogQueue)
            newBacklogQueue = [elem for i, elem in enumerate(self.backlogQueue) if i not in sent]
            self.backlogQueue = newBacklogQueue
            heapq.heapify(self.backlogQueue)

            if len(self.backlogQueue) > 0:
                # Backpressure mode. Wait for the least amount of time
                # necessary until at least one rate limiter is expected
                # to be available
                # NOTE: because our queue is no longer a FIFO, we do not want to hold for minDurationWait
                # regardless since another request with higher priority might arrive and there might be enough
                # credits. So we associate a timer to signal on self.enqueueingEvent after minDurationWait
                if round(minDurationToWait, 9) == 0.0:
                    critical("simulation enters an infinite loop!")
                    #FIXME, for now we'll just solve this by setting it to 1 millsecond
                    minDurationToWait = 1
                    #assert False
                timer = CreditsClientBacklogTimer()
                Simulation.activate(timer,
                                    timer.run(self.enqueueingEvent, minDurationToWait),
                                    at=Simulation.now())

                # TODO: is the fix below necessary? Leave it commented out for now

                # NOTE: In principle, these 2 lines below would not be
                # necessary because the rate limiter would have exactly 1
                # token after minDurationWait. However, due to
                # floating-point arithmetic precision we might not have 1
                # token and this would cause the simulation to enter an
                # almost infinite loop. These 2 lines by-pass this problem.

                # self.client.rateLimiters[minSrv].tokens = futureTokens
                # self.client.rateLimiters[minSrv].lastSent =\
                #     Simulation.now()
                # minSrv = None

class CreditsClientBacklogTimer(Simulation.Process):
    def __init__(self):
        Simulation.Process.__init__(self, name='CreditsClientBacklogTimer')

    def run(self, event, delay):
        yield Simulation.hold, self, delay
        event.signal()

class DeliverRequestWithDelay(Simulation.Process):
    def __init__(self):
        Simulation.Process.__init__(self, name='DeliverRequestWithDelay')

    def run(self, req, delay, srv):
        yield Simulation.hold, self, delay
        req.serverReceiptTime = Simulation.now()
        srv.enqueue(req)


class DeliverResponseWithDelay(Simulation.Process):
    def __init__(self):
        Simulation.Process.__init__(self, name='DeliverResponseWithDelay')

    def run(self, req, delay, srv):
        yield Simulation.hold, self, delay
        client = req.sendingClient
        expected = round(req.start + req.waitingTime + delay + req.serviceTime, 6)
        now = round(Simulation.now(), 6)
        if now != expected:
            client.log.info("id %s" % req.id)
            client.log.info("now %f" % Simulation.now())
            client.log.info("expected %f" % (req.start + req.waitingTime + delay + req.serviceTime))
            client.log.info("start %f" % req.start)
            client.log.info("waitingTime %f" % req.waitingTime)
            client.log.info("serviceTime %f" % req.serviceTime)
            client.log.info("delay %f" % delay)
        assert now == expected
        client.onRequestComplete(req, srv)
