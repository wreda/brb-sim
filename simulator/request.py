import SimPy.Simulation as Simulation


class Request():
    """A simple Request. Applications may subclass this
       for holding specific attributes if need be"""
    def __init__(self, fullId, parentId, key, replicaGroup, batchsize,
                 sendingClient, cost=1):
        self.id = fullId    # Parent request + subrequest Id
        self.parentId = parentId       # Parent request Id
        self.key = key # key on the token ring
        self.replicaGroup = replicaGroup
        self.batchsize = batchsize
        self.sendingClient = sendingClient
        self.start = Simulation.now()
        self.completionEvent = Simulation.SimEvent("ClientToServerCompletion")
        self.cost = cost
        self.waitingTime = None
        self.systemArrivalTime = None
        self.serverReceiptTime = None
        self.serverProcessTime = None
        self.sendTime = None
        self.deadline = None
        self.frg = None
        self.BN = False
        self.feedback = None

    def __repr__(self):
        return self.id

    # Used as a notifier mechanism
    def sigRequestComplete(self, piggyBack=None):
        if (self.completionEvent is not None):
            self.completionEvent.signal(piggyBack)

    def updateMetricsOnBeginProcessing(self, server):
        self.sendingClient.updateMetricsOnBeginProcessing(self, server)

    def piggybackFB(self, feedback):
        self.feedback = feedback