import SimPy.Simulation as Simulation
import math
import random
import sys
import heapq
import client
import constants
import logger
import controller
import workload
import ewma
import numpy
from collections import defaultdict

class Server():
    """A representation of a physical server that holds resources"""
    def __init__(self, id_, resourceCapacity,
                 serviceTime, serviceTimeModel,
                 customerCostModel, replicaGroups,
                 replicationFactor, leaderBalanceModel):
        self.id = id_
        self.log = self.init_logger(id_)
        self.log.debug("Create server %d" % id_)

        self.serviceTime = serviceTime
        self.serviceTimeModel = serviceTimeModel
        self.resourceCapacity = resourceCapacity
        self.processorResource = Simulation.Resource(capacity=resourceCapacity,
                                                     monitored=True)
        self.serverRRMonitor = Simulation.Monitor(name="ServerMonitor")
        self.replicaGroups = replicaGroups
        self.replicationFactor = replicationFactor
        assert len(replicaGroups) == replicationFactor
        self.customerCostModel = customerCostModel
        self.executors = None
        self.leaderBalanceModel = leaderBalanceModel

    def __repr__(self):
        return "Server%d" % self.id

    def init_logger(self, id_):
        return logger.getLogger("Server%d" % id_, constants.LOG_LEVEL)

    def start(self):
        self.executors = [Executor(i, self) for i in range(self.resourceCapacity)]

    def extend_vqueue(self, vqueue):
        # If we're using model implementations, there are no queues located at the servers
        pass

    def peek_queue(self, rg):
        assert False
        pass

    def getServiceTime(self, req):
        if (self.serviceTimeModel == "random.expovariate"):
            serviceTime = random.expovariate(1.0/(self.serviceTime*req.cost))
        elif (self.serviceTimeModel == "constant"):
            serviceTime = self.serviceTime*req.cost
        elif(self.serviceTimeModel == "math.sin"):
            serviceTime = self.serviceTime*req.cost \
                + self.serviceTime*req.cost \
                * math.sin(1 + Simulation.now()/100.0)
        else:
            self.log.critical("Unknown service time model")
            sys.exit(-1)
        return serviceTime

    def costDerivative(self, customer, qlen, deadline, arrival):
        if (self.customerCostModel == "quadratic"):
            return 2.0 * qlen
        elif (self.customerCostModel == "cubic"):
            return 3.0 * (qlen ** 2)
        elif(self.customerCostModel == "hybrid"):
            #TODO Need to figure out the integration of this
            # and reason about optimality
            return qlen/float(deadline)
        elif(self.customerCostModel == "deadline"):
            if arrival == 0:
                priority2 = float("inf")
            else:
                priority2 = 1/float(arrival)
            return (1/float(deadline), priority2)
        else:
            self.log.critical("Invalid cost derivative model")
            assert False

class Executor(Simulation.Process):

    def __init__(self, id_, server):
        self.id = id_
        self.server = server
        Simulation.Process.__init__(self,
                                    name='Executor-%s-%s' % (server.id, id_))
        Simulation.activate(self, self.run(), Simulation.now())

    def run(self):
        import global_vars
        debug = self.server.log.debug
        info = self.server.log.info
        warning = self.server.log.warning
        while (True):
            # Go through all the clients
            #   - check the queue size for replica groups that are served
            #     by this server
            #   - apply the gcmu rule to select the replica group to serve
            #   - dequeue from that replica group's virtual queue the request
            #     with the earliest deadline
            yield Simulation.hold, self

            # Check whether any request is already waiting to be processed
            # at some client
            while (True):
                debug("checking tot enqueued requests")
                tot_qlen = 0
                for replicaGroup in self.server.replicaGroups:
                    qlen = 0
                    for cl in global_vars.clients:
                        qlen += len(cl.customerQueues[replicaGroup])
                    tot_qlen += qlen
                    if qlen > 0:
                        debug("replica group %d has %d enqueued requests" % (replicaGroup, qlen))
                debug("tot enqueued requests %d" % tot_qlen)

                if tot_qlen > 0:
                    break
                # If there are no waiting requests, wait for a notification when a
                # request arrives at some client

                debug("going to yield")
                eventsToWaitOn = [event for rg, event in
                                  global_vars.customerEnqueuingEvents.iteritems()
                                  if rg in self.server.replicaGroups]
                yield Simulation.queueevent, self, eventsToWaitOn
                # for event in eventsToWaitOn:
                #     assert event.occurred == False

            maxGcmuVal = -1
            selectedRG = None
            maxQlen = -1
            for replicaGroup in self.server.replicaGroups:
                qlen = 0
                minBN = float("inf")
                arrivalTime = float("inf")
                for cl in global_vars.clients:
                    qlen += len(cl.customerQueues[replicaGroup])
                    if(len(cl.customerQueues[replicaGroup])>0):
                        minBN = min(minBN, cl.customerQueues[replicaGroup][0][0])
                        arrivalTime = cl.customerQueues[replicaGroup][0][1]
                gcmuValCli = self.server.costDerivative(replicaGroup, qlen, minBN, arrivalTime)
                if gcmuValCli > maxGcmuVal:
                    maxGcmuVal = gcmuValCli
                    selectedRG = replicaGroup
                    maxQlen = qlen
            debug("selected RG is %d with %d enqueued requests" % (selectedRG, maxQlen))
            earliestDeadline = float("inf")
            clientToServe = None

            for cl in global_vars.clients:
                customerQueue = cl.customerQueues[selectedRG]
                if (len(customerQueue) > 0):
                    # [0] is smallest element of a python heapq
                    deadline, arrivalTime, req = customerQueue[0]
                    if (deadline < earliestDeadline):
                        earliestDeadline = deadline
                        clientToServe = cl

            queueToServe = clientToServe.customerQueues[selectedRG]
            deadline, arrivalTime, req = heapq.heappop(queueToServe)
            assert req.sendingClient == clientToServe

            # DEBUG: testing with FIFO order
            # deadline, req = queueToServe.pop(0)

            req.updateMetricsOnBeginProcessing(self.server)

            debug("server %s going to serve request %s" % (self.server.id, req.id))
            if Simulation.now() != req.start + req.waitingTime:
                warning("id %s" % req.id)
                warning("now %f" % Simulation.now())
                warning("expected %f" % (req.start + req.waitingTime))
                warning("start %f" % req.start)
                warning("waitingTime %f" % req.waitingTime)
            assert Simulation.now() == req.start + req.waitingTime


            debug("%8.4f: Here I am" % self.sim.now())
            yield Simulation.request, self, self.server.processorResource
            serviceTime = self.server.getServiceTime(req)
            req.serviceTime = serviceTime
            debug("%8.4f: req %s Waited %6.3f" % (self.sim.now(), req.id, serviceTime))

            serverTime = Simulation.now()
            yield Simulation.hold, self, serviceTime
            while (Simulation.now() < serverTime + serviceTime):
                remainingTime = serverTime + serviceTime - Simulation.now()
                debug("now %f" % Simulation.now())
                debug("remainingTime %f" % remainingTime)
                yield Simulation.hold, self, remainingTime

            yield Simulation.release, self, self.server.processorResource
            debug("%8.4f: Finished" % self.sim.now())

            if Simulation.now() != req.start + req.waitingTime + req.serviceTime:
                warning("id %s" % req.id)
                warning("now %f" % Simulation.now())
                warning("expected %f" % (req.start + req.waitingTime + req.serviceTime))
                warning("start %f" % req.start)
                warning("waitingTime %f" % req.waitingTime)
                warning("serviceTime %f" % req.serviceTime)
            assert Simulation.now() == req.start + req.waitingTime + req.serviceTime

            # info("req %s serviceTime %f" % (req.id, serviceTime))

            # Multiply by two to incur the full RTT, and to avoid the
            # server being idle while pulling requests to service
            delay = 2 * constants.NW_LATENCY_BASE + \
                random.normalvariate(constants.NW_LATENCY_MU,
                                     constants.NW_LATENCY_SIGMA)
            # info("delay on req %s is %f" % (req.id, delay))
            # info("now %f" % Simulation.now())
            deliverResponse = client.DeliverResponseWithDelay()
            Simulation.activate(deliverResponse,
                                deliverResponse.run(req,
                                                    delay,
                                                    self.server),
                                at=Simulation.now())

class C3Server(Server):
    def __init__(self, id_, resourceCapacity,
             serviceTime, serviceTimeModel,
             customerCostModel, replicaGroups,
             replicationFactor, leaderBalanceModel):
        Server.__init__(self, id_, resourceCapacity,
                 serviceTime, serviceTimeModel,
                 customerCostModel, replicaGroups,
                 replicationFactor, leaderBalanceModel)
        self.replicaQueues = {rg: list() for rg in replicaGroups}
        self.enqueueingEvent = Simulation.SimEvent("Enqueuing-Event-%s" % id_)
        
    def init_logger(self, id_):
        return logger.getLogger("C3server%d" % id_, constants.LOG_LEVEL)

    def start(self):
        # Override this to avoid starting the default executors
        self.executors = [C3Executor(i, self) for i in range(self.resourceCapacity)]

    def extend_vqueue(self, vqueue):
        # If we're using leader implementations, queues are located at the servers
        for rg in self.replicaQueues:
            vqueue[rg].extend([req[2] for req in self.replicaQueues[rg]])

    def peek_queue(self, rg):
        if rg in self.replicaQueues:
            qlen = len(self.replicaQueues[rg])
            if qlen > 0:
                head_deadline = self.replicaQueues[rg][0][0]
            else:
                head_deadline = None
            return (qlen, head_deadline)
        else:
            return (0, None)

    def enqueue(self, req):
        # NOTE: we insert each request as a tuple (deadline, arrivalTime, req) because the heap algorithm
        # performs a left-to-right compare operation to determine which item has a lower value.
        # Using the arrivalTime ensures that requests that arrive later do not jump ahead simply because
        # they reside at lower memory addresses
        heapq.heappush(self.replicaQueues[req.replicaGroup], (req.deadline, req.systemArrivalTime, req))

        # DEBUG: testing with FIFO order
        # self.customerQueues[customerIndex].append( (deadline, req) )

        #print float(maxCost)*self.delay[customerIndex], float(maxCost), self.delay
        self.log.debug("server %s receiving request %s for RG %d" % (self.id, req.id, req.replicaGroup))
        self.enqueueingEvent.signal()

class C3Executor(Simulation.Process):

    def __init__(self, id_, server):
        self.id = id_
        self.server = server
        Simulation.Process.__init__(self,
                                    name='C3Executor-%s-%s' % (server.id, id_))
        Simulation.activate(self, self.run(), Simulation.now())

    def run(self):
        import global_vars
        debug = self.server.log.debug
        info = self.server.log.info
        warning = self.server.log.warning
        critical = self.server.log.critical
        while (True):
            yield Simulation.hold, self

            # Check whether any request is already waiting to be processed
            while (True):
                debug("checking tot enqueued requests")
                tot_qlen = 0
                for replicaGroup, replicaQueue in self.server.replicaQueues.iteritems():
                    qlen = len(replicaQueue)
                    tot_qlen += qlen
                    if qlen > 0:
                        debug("replica group %d has %d enqueued requests" % (replicaGroup, qlen))
                debug("tot enqueued requests %d" % tot_qlen)

                if tot_qlen > 0:
                    break
                # If there are no waiting requests, wait for a notification when a
                # request arrives at this server
                debug("going to yield")
                event = self.server.enqueueingEvent
                yield Simulation.queueevent, self, [event]
                # assert event.occurred == False

            # What replica group to serve? Use Gcmu-rule
            maxGcmuVal = -1
            selectedRG = None
            maxQlen = -1
            for replicaGroup, replicaQueue in self.server.replicaQueues.iteritems():
                qlen = len(replicaQueue)
                if qlen > 0:
                    deadline = replicaQueue[0][0]
                    gcmuValCli = self.server.costDerivative(replicaGroup, qlen, deadline, replicaQueue[0][1])
                    if gcmuValCli > maxGcmuVal:
                        maxGcmuVal = gcmuValCli
                        selectedRG = replicaGroup
                        maxQlen = qlen
            debug("selected RG is %d with %d enqueued requests" % (selectedRG, maxQlen))

            queueToServe = self.server.replicaQueues[selectedRG]
            deadline, arrivalTime, req = heapq.heappop(queueToServe)

            req.updateMetricsOnBeginProcessing(self.server)

            debug("server %s going to serve request %s" % (self.server.id, req.id))
            if Simulation.now() != req.start + req.waitingTime:
                warning("id %s" % req.id)
                warning("now %f" % Simulation.now())
                warning("expected %f" % (req.start + req.waitingTime))
                warning("start %f" % req.start)
                warning("waitingTime %f" % req.waitingTime)
            assert Simulation.now() == req.start + req.waitingTime


            debug("%8.4f: Here I am" % self.sim.now())
            yield Simulation.request, self, self.server.processorResource
            serviceTime = self.server.getServiceTime(req)
            req.serviceTime = serviceTime
            debug("%8.4f: req %s Waited %6.3f" % (self.sim.now(), req.id, serviceTime))

            serverTime = Simulation.now()
            yield Simulation.hold, self, serviceTime
            while (Simulation.now() < serverTime + serviceTime):
                remainingTime = serverTime + serviceTime - Simulation.now()
                debug("now %f" % Simulation.now())
                debug("remainingTime %f" % remainingTime)
                yield Simulation.hold, self, remainingTime

            yield Simulation.release, self, self.server.processorResource
            debug("%8.4f: Finished" % self.sim.now())

            if Simulation.now() != req.start + req.waitingTime + req.serviceTime:
                warning("id %s" % req.id)
                warning("now %f" % Simulation.now())
                warning("expected %f" % (req.start + req.waitingTime + req.serviceTime))
                warning("start %f" % req.start)
                warning("waitingTime %f" % req.waitingTime)
                warning("serviceTime %f" % req.serviceTime)
            assert Simulation.now() == req.start + req.waitingTime + req.serviceTime
            
            queueSizeAfter = len(self.server.processorResource.waitQ)
            # info("req %s serviceTime %f" % (req.id, serviceTime))
            feedback = {"waitingTime": req.waitingTime,
                        "serviceTime": req.serviceTime,
                        "queueSizeAfter": queueSizeAfter}
            req.piggybackFB(feedback)
            delay = constants.NW_LATENCY_BASE + \
                random.normalvariate(constants.NW_LATENCY_MU,
                                     constants.NW_LATENCY_SIGMA)
            deliverResponse = client.DeliverResponseWithDelay()
            Simulation.activate(deliverResponse,
                                deliverResponse.run(req,
                                                    delay,
                                                    self.server),
                                at=Simulation.now())
        
class ReplicaLeaderServer(Server):
    def __init__(self, id_, resourceCapacity,
                 serviceTime, serviceTimeModel,
                 customerCostModel, replicaGroups,
                 replicationFactor, leaderBalanceModel):
        Server.__init__(self, id_, resourceCapacity,
                 serviceTime, serviceTimeModel,
                 customerCostModel, replicaGroups,
                 replicationFactor, leaderBalanceModel)
        self.replicaQueues = {rg: list() for rg in replicaGroups}
        self.enqueueingEvent = Simulation.SimEvent("Enqueuing-Event-%s" % id_)
        self.leaderOfGroup = replicaGroups[0]

        # State for round robin selection
        self.roundRobinPicks = {rg: random.randrange(0, replicationFactor) for rg in replicaGroups}

    def init_logger(self, id_):
        return logger.getLogger("RLserver%d" % id_, constants.LOG_LEVEL)

    def start(self):
        # Override this to avoid starting the default executors
        self.executors = [ReplicaLeaderExecutor(i, self) for i in range(self.resourceCapacity)]

    def extend_vqueue(self, vqueue):
        # If we're using leader implementations, queues are located at the servers
        for rg in self.replicaQueues:
            vqueue[rg].extend([req[2] for req in self.replicaQueues[rg]])

    def peek_queue(self, rg):
        if rg in self.replicaQueues:
            qlen = len(self.replicaQueues[rg])
            if qlen > 0:
                head_deadline = self.replicaQueues[rg][0][0]
            else:
                head_deadline = None
            return (qlen, head_deadline)
        else:
            return (0, None)

    def enqueue(self, req):
        # NOTE: we insert each request as a tuple (deadline, arrivalTime, req) because the heap algorithm
        # performs a left-to-right compare operation to determine which item has a lower value.
        # Using the arrivalTime ensures that requests that arrive later do not jump ahead simply because
        # they reside at lower memory addresses
        heapq.heappush(self.replicaQueues[req.replicaGroup], (req.deadline, req.systemArrivalTime, req))

        # DEBUG: testing with FIFO order
        # self.customerQueues[customerIndex].append( (deadline, req) )

        #print float(maxCost)*self.delay[customerIndex], float(maxCost), self.delay
        self.log.debug("server %s receiving request %s for RG %d" % (self.id, req.id, req.replicaGroup))
        self.enqueueingEvent.signal()

class ReplicaLeaderExecutor(Simulation.Process):

    def __init__(self, id_, server):
        self.id = id_
        self.server = server
        Simulation.Process.__init__(self,
                                    name='ReplicaLeaderExecutor-%s-%s' % (server.id, id_))
        Simulation.activate(self, self.run(), Simulation.now())

    def run(self):
        import global_vars
        debug = self.server.log.debug
        info = self.server.log.info
        warning = self.server.log.warning
        critical = self.server.log.critical
        while (True):
            yield Simulation.hold, self

            # Check whether any request is already waiting to be processed
            while (True):
                debug("checking tot enqueued requests")
                tot_qlen = 0
                for replicaGroup, replicaQueue in self.server.replicaQueues.iteritems():
                    qlen = len(replicaQueue)
                    tot_qlen += qlen
                    if qlen > 0:
                        debug("replica group %d has %d enqueued requests" % (replicaGroup, qlen))
                debug("tot enqueued requests %d" % tot_qlen)

                if tot_qlen > 0:
                    break
                # If there are no waiting requests, wait for a notification when a
                # request arrives at this server
                debug("going to yield")
                event = self.server.enqueueingEvent
                yield Simulation.queueevent, self, [event]
                # assert event.occurred == False

            # What replica group to serve? Use Gcmu-rule
            maxGcmuVal = -1
            selectedRG = None
            maxQlen = -1
            for replicaGroup, replicaQueue in self.server.replicaQueues.iteritems():
                qlen = len(replicaQueue)
                if qlen > 0:
                    deadline = replicaQueue[0][0]
                    gcmuValCli = self.server.costDerivative(replicaGroup, qlen, deadline, replicaQueue[0][1])
                    if gcmuValCli > maxGcmuVal:
                        maxGcmuVal = gcmuValCli
                        selectedRG = replicaGroup
                        maxQlen = qlen
            debug("selected RG is %d with %d enqueued requests" % (selectedRG, maxQlen))

            queueToServe = self.server.replicaQueues[selectedRG]
            deadline, arrivalTime, req = heapq.heappop(queueToServe)

            if selectedRG == self.server.leaderOfGroup:
                debug("serving RG %d that this server is leader of" % selectedRG)
                # Which replica server node to use
                if self.server.leaderBalanceModel == "RR":
                    rrIdx = self.server.roundRobinPicks[selectedRG]
                    self.server.roundRobinPicks[selectedRG] = (self.server.roundRobinPicks[selectedRG] + 1) % self.server.replicationFactor
                    replicaToServe = global_vars.serversByReplicaGroup[selectedRG][rrIdx]
                # TODO: this is basically C3 that we could use here!!!
                # TODO: least-outstanding requests load balancing
                # Note: this requires knowledge of the queue size at the remote replica server
                # Do we get this information from servers periodically?
                else:
                    critical("invalid leaderBalanceModel")

                # revise the Gc\mu-rule to do choose RG
                if replicaToServe != self.server:
                    debug("leader sending the request %s to server %s" % (req.id, replicaToServe.id))

                    delay = constants.NW_LATENCY_BASE + \
                        random.normalvariate(constants.NW_LATENCY_MU,
                                             constants.NW_LATENCY_SIGMA)
                    deliver = client.DeliverRequestWithDelay()
                    Simulation.activate(deliver,
                                        deliver.run(req, delay, replicaToServe),
                                        at=Simulation.now())
                    continue

            req.updateMetricsOnBeginProcessing(self.server)

            debug("server %s going to serve request %s" % (self.server.id, req.id))
            if Simulation.now() != req.start + req.waitingTime:
                warning("id %s" % req.id)
                warning("now %f" % Simulation.now())
                warning("expected %f" % (req.start + req.waitingTime))
                warning("start %f" % req.start)
                warning("waitingTime %f" % req.waitingTime)
            assert Simulation.now() == req.start + req.waitingTime


            debug("%8.4f: Here I am" % self.sim.now())
            yield Simulation.request, self, self.server.processorResource
            serviceTime = self.server.getServiceTime(req)
            req.serviceTime = serviceTime
            debug("%8.4f: req %s Waited %6.3f" % (self.sim.now(), req.id, serviceTime))

            serverTime = Simulation.now()
            yield Simulation.hold, self, serviceTime
            while (Simulation.now() < serverTime + serviceTime):
                remainingTime = serverTime + serviceTime - Simulation.now()
                debug("now %f" % Simulation.now())
                debug("remainingTime %f" % remainingTime)
                yield Simulation.hold, self, remainingTime

            yield Simulation.release, self, self.server.processorResource
            debug("%8.4f: Finished" % self.sim.now())

            if Simulation.now() != req.start + req.waitingTime + req.serviceTime:
                warning("id %s" % req.id)
                warning("now %f" % Simulation.now())
                warning("expected %f" % (req.start + req.waitingTime + req.serviceTime))
                warning("start %f" % req.start)
                warning("waitingTime %f" % req.waitingTime)
                warning("serviceTime %f" % req.serviceTime)
            assert Simulation.now() == req.start + req.waitingTime + req.serviceTime

            # info("req %s serviceTime %f" % (req.id, serviceTime))

            delay = constants.NW_LATENCY_BASE + \
                random.normalvariate(constants.NW_LATENCY_MU,
                                     constants.NW_LATENCY_SIGMA)
            deliverResponse = client.DeliverResponseWithDelay()
            Simulation.activate(deliverResponse,
                                deliverResponse.run(req,
                                                    delay,
                                                    self.server),
                                at=Simulation.now())

class CreditsServer(Server):
    def __init__(self, id_, resourceCapacity,
                 serviceTime, serviceTimeModel,
                 customerCostModel, replicaGroups,
                 replicationFactor, leaderBalanceModel):
        Server.__init__(self, id_, resourceCapacity,
                 serviceTime, serviceTimeModel,
                 customerCostModel, replicaGroups,
                 replicationFactor, leaderBalanceModel)

        self.throughput = ewma.EWMA()
        self.utilizations = defaultdict(int)
        self.replicaQueues = {rg: list() for rg in replicaGroups}
        self.enqueueingEvent = Simulation.SimEvent("Enqueuing-Event-%s" % id_)

    def init_logger(self, id_):
        return logger.getLogger("CreditsServer%d" % id_, constants.LOG_LEVEL)

    def start(self):
        # Override this to avoid starting the default executors
        self.executors = [CreditsExecutor(i, self) for i in range(self.resourceCapacity)]
        self.reporter = CreditsReporter(self)

    def extend_vqueue(self, vqueue):
        # If we're using credits implementations, queues are located at the servers
        for rg in self.replicaQueues:
            vqueue[rg].extend([req[2] for req in self.replicaQueues[rg]])

    def peek_queue(self, rg):
        if rg in self.replicaQueues:
            qlen = len(self.replicaQueues[rg])
            if qlen > 0:
                head_deadline = self.replicaQueues[rg][0][0]
            else:
                head_deadline = None
            return (qlen, head_deadline)
        else:
            return (0, None)

    def enqueue(self, req):
        # NOTE: we insert each request as a tuple (deadline, arrivalTime, req) because the heap algorithm
        # performs a left-to-right compare operation to determine which item has a lower value.
        # Using the arrivalTime ensures that requests that arrive later do not jump ahead simply because
        # they reside at lower memory addresses
        heapq.heappush(self.replicaQueues[req.replicaGroup], (req.deadline, req.systemArrivalTime, req))

        self.utilizations[req.sendingClient] += req.cost

        # DEBUG: testing with FIFO order
        # self.customerQueues[customerIndex].append( (deadline, req) )

        #print float(maxCost)*self.delay[customerIndex], float(maxCost), self.delay
        self.log.debug("server %s receiving request %s for RG %d" % (self.id, req.id, req.replicaGroup))
        self.enqueueingEvent.signal()

class CreditsReporter(Simulation.Process):

    def __init__(self, server):
        self.server = server
        Simulation.Process.__init__(self,
                                    name='CreditsReporter-%s' % (server.id))
        Simulation.activate(self, self.run(), Simulation.now())

    def run(self):
        import global_vars

        interval = constants.CTRL_INTERVAL

        debug = self.server.log.debug
        info = self.server.log.info
        warning = self.server.log.warning
        critical = self.server.log.critical

        while (workload.finished() == False):
            yield Simulation.hold, self, interval
            # debug("reporting congestion feedback")

            utilizations = defaultdict(int)
            throughput = self.server.throughput.getMean()

            for cli in global_vars.clients:
                utilizations[cli] = self.server.utilizations[cli]
                self.server.utilizations[cli] = 0

            # DEBUG
            # global_vars.controller.enqueueCongestionFeedback(self.server, throughput, utilizations)

            delay = constants.NW_LATENCY_BASE + \
                random.normalvariate(constants.NW_LATENCY_MU,
                                     constants.NW_LATENCY_SIGMA)
            feedback = controller.DeliverCongestionFeedbackWithDelay()
            Simulation.activate(feedback,
                                feedback.run(self.server, throughput, utilizations, delay, global_vars.controller),
                                at=Simulation.now())



class CreditsExecutor(Simulation.Process):

    def __init__(self, id_, server):
        self.id = id_
        self.server = server
        Simulation.Process.__init__(self,
                                    name='CreditsExecutor-%s-%s' % (server.id, id_))
        Simulation.activate(self, self.run(), Simulation.now())

    def run(self):
        import global_vars
        debug = self.server.log.debug
        info = self.server.log.info
        warning = self.server.log.warning
        critical = self.server.log.critical
        while (True):
            yield Simulation.hold, self

            # Check whether any request is already waiting to be processed
            while (True):
                # debug("checking tot enqueued requests")
                tot_qlen = 0
                for replicaGroup, replicaQueue in self.server.replicaQueues.iteritems():
                    qlen = len(replicaQueue)
                    tot_qlen += qlen
                    if qlen > 0:
                        debug("replica group %d has %d enqueued requests" % (replicaGroup, qlen))
                # debug("tot enqueued requests %d" % tot_qlen)

                if tot_qlen > 0:
                    debug("tot enqueued requests %d" % tot_qlen)
                    break
                # If there are no waiting requests, wait for a notification when a
                # request arrives at this server
                # debug("going to yield")
                event = self.server.enqueueingEvent
                yield Simulation.queueevent, self, [event]
                # assert event.occurred == False


            # What replica group to serve? Use Gcmu-rule
            maxGcmuVal = -1
            selectedRG = None
            maxQlen = -1
            for replicaGroup, replicaQueue in self.server.replicaQueues.iteritems():
                qlen = len(replicaQueue)
                if qlen > 0:
                    deadline = replicaQueue[0][0]
                    gcmuValCli = self.server.costDerivative(replicaGroup, qlen, deadline, replicaQueue[0][1])
                    if gcmuValCli > maxGcmuVal:
                        maxGcmuVal = gcmuValCli
                        selectedRG = replicaGroup
                        maxQlen = qlen
            debug("selected RG is %d with %d enqueued requests" % (selectedRG, maxQlen))

            queueToServe = self.server.replicaQueues[selectedRG]
            deadline, arrivalTime, req = heapq.heappop(queueToServe)

            req.updateMetricsOnBeginProcessing(self.server)

            debug("server %s going to serve request %s" % (self.server.id, req.id))
            if Simulation.now() != req.start + req.waitingTime:
                warning("id %s" % req.id)
                warning("now %f" % Simulation.now())
                warning("expected %f" % (req.start + req.waitingTime))
                warning("start %f" % req.start)
                warning("waitingTime %f" % req.waitingTime)
            assert Simulation.now() == req.start + req.waitingTime


            debug("%8.4f: Here I am" % self.sim.now())
            yield Simulation.request, self, self.server.processorResource
            serviceTime = self.server.getServiceTime(req)
            req.serviceTime = serviceTime
            debug("%8.4f: req %s Waited %6.3f" % (self.sim.now(), req.id, serviceTime))

            serverTime = Simulation.now()
            yield Simulation.hold, self, serviceTime
            while (Simulation.now() < serverTime + serviceTime):
                remainingTime = serverTime + serviceTime - Simulation.now()
                debug("now %f" % Simulation.now())
                debug("remainingTime %f" % remainingTime)
                yield Simulation.hold, self, remainingTime

            yield Simulation.release, self, self.server.processorResource
            debug("%8.4f: Finished" % self.sim.now())

            rate = req.cost / serviceTime
            self.server.throughput.update(rate)

            if Simulation.now() != req.start + req.waitingTime + req.serviceTime:
                warning("id %s" % req.id)
                warning("now %f" % Simulation.now())
                warning("expected %f" % (req.start + req.waitingTime + req.serviceTime))
                warning("start %f" % req.start)
                warning("waitingTime %f" % req.waitingTime)
                warning("serviceTime %f" % req.serviceTime)
            assert Simulation.now() == req.start + req.waitingTime + req.serviceTime

            # info("req %s serviceTime %f" % (req.id, serviceTime))

            delay = constants.NW_LATENCY_BASE + \
                random.normalvariate(constants.NW_LATENCY_MU,
                                     constants.NW_LATENCY_SIGMA)
            deliverResponse = client.DeliverResponseWithDelay()
            Simulation.activate(deliverResponse,
                                deliverResponse.run(req,
                                                    delay,
                                                    self.server),
                                at=Simulation.now())
