import math
import SimPy.Simulation as Simulation

class EWMA():
    def __init__(self):
        self.n = 0
        self.mean = 0
        self.s = 0
        self.u = 0
        self.var = 0
        self.alpha = 0.6
        self.lastUpdate = 0
        self.stalenessThreshold = 10
    def update(self, s):
        self.lastUpdate = Simulation.now()
        self.n += 1
        self.mean = s*self.alpha + (1-self.alpha)*self.mean
        self.var = (1-self.alpha)*self.var + (self.alpha) * (self.u**2)
        if(self.s == 0):
            self.u = 0
        else:
            self.u = (s-self.s)/self.s
        self.s = s

    def getMean(self):
        return self.mean

    def getVariance(self):
        if self.n < 2:
            return 0
        return self.var

    def getVolatility(self):
        sd = math.sqrt(self.getVariance())
        return (sd*100.0)

    def isStale(self):
        if((Simulation.now() - self.lastUpdate) > self.stalenessThreshold and self.n>0):
            return True
        
    def wipe(self):
        self.n = 0
        self.mean = 0
        self.s = 0
        self.u = 0
        self.var = 0
        self.lastUpdate = 0
        
    def __repr__(self):
        return str(self.getMean())

    def __str__(self):
        return str(self.getMean())