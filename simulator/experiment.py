# import SimPy.SimulationTrace
import SimPy.Simulation as Simulation
import collections
import server
import client
import workload
import workloaddef
import argparse
import random
import constants
import numpy
import global_vars
import os
import logger
import constants
import logger
import logging
import math
import statCollector
import time
import datetime
import controller
import signal
import sys
from collections import defaultdict

def printMonitorTimeSeriesToFile(fileDesc, prefix, monitor):
    for entry in monitor:
        fileDesc.write("%s %s %s\n" % (prefix, entry[0], entry[1]))

def sigterm_handler(_signo, _stack_frame):
    log.warning("SIGTERM caught")
    Simulation.stopSimulation()

def sigint_handler(_signo, _stack_frame):
    log.warning("SIGINT caught")
    Simulation.stopSimulation()

def runExperiment(args, log):
    servers = []
    clients = []
    workloadGens = []
    # Set the random seed
    random.seed(args.seed)
    numpy.random.seed(args.seed)
    
    # Print out experiment input parameters
    paramsFD = open("../%s/InputParams" % args.logFolder, 'w')
    paramsFD.write('    '.join(args.__dict__.keys()) + '\n')
    paramsFD.write('    '.join(map(lambda x : str(x), args.__dict__.values())) + '\n')
    paramsFD.close()

    Simulation.initialize()

    constants.NW_LATENCY_BASE = args.nwLatencyBase
    constants.NW_LATENCY_MU = args.nwLatencyMu
    constants.NW_LATENCY_SIGMA = args.nwLatencySigma

    constants.CTRL_INTERVAL = args.ctrlInterval

    assert args.expScenario != ""
    
    if args.impl == "model":
        srvClass = server.Server
        cliClass = client.Client
    elif args.impl == "leader":
        srvClass = server.ReplicaLeaderServer
        cliClass = client.ReplicaLeaderClient
    elif args.impl == "credits":
        srvClass = server.CreditsServer
        cliClass = client.CreditsClient
    elif args.impl == "rs":
        srvClass = server.C3Server
        cliClass = client.C3Client
    else:
        log.critical("invalid impl strategy %s" % args.impl)
        assert False

    if (args.expScenario == "base"):
        serversByReplicaGroup = collections.defaultdict(list)
        # Start the servers
        for i in range(args.numServers):

            # The servers need to know the replica groups they are a part
            # of in order to poll correctly
            replicaGroups = []
            # Pick a node and it's next RF - 1 number of neighbours
            for j in range(args.replicationFactor):
                rg = (args.numServers + i - j) % args.numServers
                replicaGroups.append(rg)
            serv = srvClass(id_=i,
                             resourceCapacity=args.serverConcurrency,
                             serviceTime=(args.serviceTimeParam),
                             serviceTimeModel=args.serviceTimeModel,
                             customerCostModel=args.customerCostModel,
                             replicaGroups=replicaGroups,
                             replicationFactor=args.replicationFactor,
                             leaderBalanceModel=args.leaderBalanceModel)
            serv.start()
            servers.append(serv)
            for rg in replicaGroups:
                serversByReplicaGroup[rg].append(serv)
    else:
        log.critical("Invalid exp scenario")
        assert False

    baseDemandWeight = 1.0
    assert args.highDemandFraction >= 0 and args.highDemandFraction < 1.0
    assert args.demandSkew >= 0 and args.demandSkew < 1.0
    assert not (args.demandSkew == 0 and args.highDemandFraction != 0)
    assert not (args.demandSkew != 0 and args.highDemandFraction == 0)

    if(args.highDemandFraction > 0.0 and args.demandSkew >= 0):
        heavyClientWeight = baseDemandWeight * \
            args.demandSkew / args.highDemandFraction
        numHeavyClients = int(args.highDemandFraction * args.numClients)
        heavyClientWeights = [heavyClientWeight] * numHeavyClients

        lightClientWeight = baseDemandWeight * \
            (1 - args.demandSkew) / (1 - args.highDemandFraction)
        numLightClients = args.numClients - numHeavyClients
        lightClientWeights = [lightClientWeight] * numLightClients
        clientWeights = heavyClientWeights + lightClientWeights
    else:
        clientWeights = [baseDemandWeight] * args.numClients

    assert sum(clientWeights) > 0.99 * args.numClients
    assert sum(clientWeights) <= args.numClients

    latencyMonitor = Simulation.Monitor(name="Latency")

    # Start the clients
    for i in range(args.numClients):
        c = cliClass(id_=i,
                      latencyMonitor=latencyMonitor,
                      serverList=servers,
                      serversByReplicaGroup=serversByReplicaGroup,
                      replicationFactor=args.replicationFactor,
                      shadowReadRatio=args.shadowReadRatio,
                      demandWeight=clientWeights[i],
                      deadlineModel=args.deadlineModel,
                      serviceTime=args.serviceTimeParam,
                      cubicC=args.cubicC,
                      cubicSmax=args.cubicSmax,
                      cubicBeta=args.cubicBeta,
                      rateInterval=args.rateInterval,
                      concurrencyWeight=args.concurrencyWeight,
                      hysterisisFactor=args.hysterisisFactor,
                      selectionStrategy=args.selectionStrategy,
                      backpressure=args.backpressure,
                      costExponent = args.costExponent)
        clients.append(c)


    if (args.impl == "credits"):
        global_vars.controller = controller.Controller(args.ctrlInterval, servers, clients, serversByReplicaGroup)

    if args.workloadModel == "synthetic":
        workloadDef = workloaddef.SyntheticWorkloadDef(numRequests=args.numRequests, numRgs=len(servers),
                                                       reqCostModel=args.reqCostModel, reqCostParam=args.reqCostParam,
                                                       accessPatternModel=args.accessPatternModel, accessPatternParam=args.accessPatternParam,
                                                       batchSizeModel=args.batchSizeModel, batchSizeParam=args.batchSizeParam,
                                                       valueSizeModel=args.valueSizeModel, valueSizeParam=args.valueSizeParam,
                                                       logFolder=args.logFolder, expPrefix=args.expPrefix)
    elif args.workloadModel == "trace":
        workloadDef = workloaddef.TraceWorkloadDef(traceFile=args.workloadTrace, numRequests=args.numRequests, numRgs=len(servers),
                                                   reqCostModel=args.reqCostModel, reqCostParam=args.reqCostParam,
                                                   accessPatternModel=args.accessPatternModel, accessPatternParam=args.accessPatternParam,
                                                   batchSizeModel=args.batchSizeModel, batchSizeParam=args.batchSizeParam,
                                                   valueSizeModel=args.valueSizeModel, valueSizeParam=args.valueSizeParam,
                                                   logFolder=args.logFolder, expPrefix=args.expPrefix, seed=args.seed)
    else:
        log.critical("Invalid workloadModel")
        assert False

    assert args.expScenario == "base"  # TODO: other experiment scenarios
    # This is where we set the inter-arrival times based on
    # the required utilization level and the service time
    # of the overall server pool.

    valueSizeAvg = workloadDef.avgValueSize
    costAvg = workloadDef.avgCost
    batchSizeAvg = workloadDef.batchSizeParam
    avgSysRate = args.numServers * args.serverConcurrency / float(args.serviceTimeParam)
    # For trace workloads, we calculate our arrival rate as if we're zipfian
    if (args.accessPatternModel == "uniform" and not args.workloadModel == "trace"):
        arrivalRate = args.utilization * avgSysRate / \
            (costAvg * batchSizeAvg)

    elif (args.accessPatternModel == "zipfian" or args.workloadModel == "trace"):
        # FIXME: how to do this with a trace workload?
        maxCost = 0
        maxCostRG = 0
        costsPerServer = {s:0 for s in servers}
        for replicaGroup, totCost in workloadDef.totCostPerRG.iteritems():
            for s in serversByReplicaGroup[replicaGroup]:
                costsPerServer[s] += totCost / float(len(serversByReplicaGroup[replicaGroup]))
        for s, totCost in costsPerServer.iteritems():
            if(totCost > maxCost):
                maxCost = totCost
                maxCostS = s

        # calculate maxCost for individual servers
        
        # FIXME Increased replication factor doesn't necessarily imply more capacity
        # Need to reason about how to include this in our formula
        # maxCost /= float(args.replicationFactor) # Assume work is dived equally among the replica servers
        print maxCost
        arrivalRate = args.utilization * args.serverConcurrency / (maxCost / float(workloadDef.numRequests) * args.serviceTimeParam)
        # log.info(totCostPerRG)
        # log.info("replicaGroup %d has max cost %d" % (replicaGroup, maxCost))
    else:
        log.critical("Invalid exp scenario")
        assert False

    interArrivalTime = 1 / float(arrivalRate)
    log.info("avg service time %f" % args.serviceTimeParam)
    log.info("avg system service rate %f" % avgSysRate)
    log.info("avg inter-arrival time %f" % interArrivalTime)
    log.info("avg inter-arrival rate %f" % arrivalRate)
    log.info("avg request cost %f" % costAvg)
    log.info("avg value size %f" % valueSizeAvg)
    log.info("avg batch size %f" % batchSizeAvg)

    if args.shadowReadRatio > 0:
        log.warn("We cannot compute the inter arrival rate if shadow reads are enabled!")

    # Start workload generators (analogous to YCSB)
    loadedRequests = 0
    for i in range(args.numWorkload):
        if(i == args.numWorkload - 1):
            sz = args.numRequests - loadedRequests
        else:
            sz = args.numRequests / args.numWorkload
        batchDefSlice = workloadDef.getSlice(loadedRequests, loadedRequests + sz)
        loadedRequests += sz      
        w = workload.Workload(id_=i,
                              clientList=clients,
                              serverList=servers,
                              batchDefList=batchDefSlice,
                              interArrivalModel=args.interArrivalModel,
                              interArrivalParam=interArrivalTime * args.numWorkload)  # NOTE: this is the inter-arrival time, not the rate!
        Simulation.activate(w, w.run(),
                            at=0.0),
        workloadGens.append(w)

    customerEnqueuingEvents = {serv.id: Simulation.SimEvent(
                               "Enqueuing-Event-%s" % serv.id) for
                               serv in servers}

    sc = statCollector.StatCollector(workloadDef.numRequests, clients, servers, serversByReplicaGroup, workloadGens, \
                                     len(servers), args.starvPrevention, args.starvParam, \
                                     args.deadlineModel, args.workloadModel, log)
    Simulation.activate(sc, sc.run(10), at=0.0)

    global_vars.servers = servers
    global_vars.clients = clients
    global_vars.workloadGens = workloadGens
    global_vars.customerEnqueuingEvents = customerEnqueuingEvents
    global_vars.serversByReplicaGroup = serversByReplicaGroup


    # Setup signal handlers
    signal.signal(signal.SIGTERM, sigterm_handler)
    signal.signal(signal.SIGINT, sigint_handler)
    # Begin simulation
    Simulation.simulate(until=args.simulationDuration)
    
    #
    # Print a bunch of timeseries
    #
    pendingRequestsFD = open("../%s/%s_PendingRequests" % 
                             (args.logFolder,
                              args.expPrefix), 'w')
    actMonFD = open("../%s/%s_ActMon" % (args.logFolder,
                                         args.expPrefix), 'w')
    taskLatencyFD = open("../%s/%s_taskLatency" % (args.logFolder,
                                           args.expPrefix), 'w')
    requestLatencyFD = open("../%s/%s_requestLatency" % 
                         (args.logFolder, args.expPrefix), 'w')
    serverRRFD = open("../%s/%s_serverRR" % (args.logFolder,
                                             args.expPrefix), 'w')    
    vQueueLenFD = open("../%s/%s_vQueueLen" % (args.logFolder,
                                             args.expPrefix), 'w')
    frgFD = open("../%s/%s_frg" % (args.logFolder,
                                   args.expPrefix), 'w')
    frgVarFD = open("../%s/%s_frgVar" % (args.logFolder,
                                   args.expPrefix), 'w')
    intraBatchRTRSDFD = open("../%s/%s_rtrSD" % (args.logFolder,
                                   args.expPrefix), 'w')
    greedyNonMonotonicFD = open("../%s/%s_greedyNonMonotonic" % (args.logFolder,
                                   args.expPrefix), 'w')
    intraBatchRTRSD = []
    falseBottleneckEstCount = 0
    for clientNode in clients:
        printMonitorTimeSeriesToFile(pendingRequestsFD,
                                     clientNode.id,
                                     clientNode.pendingRequestsMonitor)
        printMonitorTimeSeriesToFile(requestLatencyFD,
                                     clientNode.id,
                                     clientNode.requestLatencyMonitor)
        printMonitorTimeSeriesToFile(frgFD,
                                     clientNode.id,
                                     clientNode.frgMonitor)
        printMonitorTimeSeriesToFile(frgVarFD,
                                     clientNode.id,
                                     clientNode.frgVarMonitor)
        printMonitorTimeSeriesToFile(intraBatchRTRSDFD,
                                     clientNode.id,
                                     clientNode.intraBatchRTRSDMonitor)
        printMonitorTimeSeriesToFile(greedyNonMonotonicFD,
                                     clientNode.id,
                                     clientNode.greedyNonMonotonicityMonitor)
        intraBatchRTRSD.extend([entry[1] for entry in clientNode.intraBatchRTRSDMonitor])
        falseBottleneckEstCount += clientNode.falseBottleneckEstCount
        if(args.impl == "rs" and args.backpressure):
            log.info("------- Client:%s ------"%clientNode.id)
            log.info("Send Rate: %f" % clientNode.rateMonitor.mean())
            log.info("Receive Rate: %f" % clientNode.receiveRateMonitor.mean())
        
    for serv in servers:
        printMonitorTimeSeriesToFile(actMonFD,
                                     serv.id,
                                     serv.processorResource.actMon)
        printMonitorTimeSeriesToFile(serverRRFD,
                                     serv.id,
                                     serv.serverRRMonitor)

        log.info("------- Server:%s %s ------" % (serv.id, "ActMon"))
        log.info("RGs:%s" % serv.replicaGroups)
        log.info("Mean: %f" % serv.processorResource.actMon.mean())

    # print args.numRequests, len(latencyMonitor)
    # assert args.numRequests == len(latencyMonitor)

    log.info("------- Latency ------")
    log.info("Mean Latency: %f" % \
        (sum([float(entry[1].split()[0]) for entry in latencyMonitor])\
        / float(len(latencyMonitor))))
    log.info("Median Latency: %f" % \
        (numpy.median(numpy.array([float(entry[1].split()[0]) for entry in latencyMonitor]))))
    log.info("95 percentile Latency: %f" % \
        (numpy.percentile(numpy.array([float(entry[1].split()[0]) for entry in latencyMonitor]), 95)))
    log.info("99 percentile Latency: %f" % \
        (numpy.percentile(numpy.array([float(entry[1].split()[0]) for entry in latencyMonitor]), 99)))

    log.info("------ Misc ------")    
    log.info("Intra-Batch RT RSD %f" % numpy.mean(intraBatchRTRSD))
    log.info("Percentage False BN Estimations %f" % (float(falseBottleneckEstCount) / args.numRequests * 100))
    
    printMonitorTimeSeriesToFile(taskLatencyFD, "0",
                                 latencyMonitor)
    log.info("------- VQ Sizes ------")
    for rg in sc.vQueueLenMonitor.keys():
        log.info("RG:%d Mean Queue Length: %f" % \
        (rg, sum([float(entry[1]) for entry in sc.vQueueLenMonitor[rg]])\
        / float(len(sc.vQueueLenMonitor[rg]))))
    log.info("Variance Queue Length: %f" % \
        sc.vQueueVarMonitor.mean())

    for rg in sc.vQueueLenMonitor.keys():
        printMonitorTimeSeriesToFile(vQueueLenFD, rg,
                                     sc.vQueueLenMonitor[rg])
    
    if args.interArrivalModel == "poisson":
        # As a sanity check, we compute the mean, median, 95, and 99 percentile of waiting time
        # of a M/M/1 queueing system with the same arrival rate and with a service rate
        # equal to the system aggregate service rate
        def perc_waiting_time(m, r):
            return m * math.log(100 / (100 - r))

        mean = 1 / (avgSysRate - arrivalRate * args.numWorkload)
        p50 = perc_waiting_time(mean, 50)
        p95 = perc_waiting_time(mean, 95)
        p99 = perc_waiting_time(mean, 99)
        log.info("------- M/M/1 queuing system analysis -------")
        log.info("Mean Latency: %f" % mean)
        log.info("Median Latency: %f" % p50)
        log.info("95 percentile Latency: %f" % p95)
        log.info("99 percentile Latency: %f" % p99)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Cicero sim.')
    parser.add_argument('--numClients', nargs='?',
                        type=int, default=1)
    parser.add_argument('--numServers', nargs='?',
                        type=int, default=1)
    parser.add_argument('--numWorkload', nargs='?',
                        type=int, default=1)
    parser.add_argument('--impl', nargs='?',
                        type=str, default="model")
    parser.add_argument('--serverConcurrency', nargs='?',
                        type=int, default=1)
    parser.add_argument('--customerCostModel', nargs='?',
                        type=str, default="quadratic")
    parser.add_argument('--deadlineModel', nargs='?',
                        type=str, default="equalMaxCost")
    parser.add_argument('--valueSizeModel', nargs='?',
                        type=str, default="constant")
    parser.add_argument('--valueSizeParam', nargs='?',
                        type=float, default=0)
    parser.add_argument('--starvPrevention', nargs='?',
                        type=str, default='disabled')
    parser.add_argument('--starvParam', nargs='?',
                        type=float, default=1000)
    parser.add_argument('--interArrivalModel', nargs='?',
                        type=str, default="constant")
    parser.add_argument('--utilization', nargs='?',
                        type=float, default=0.90)
    parser.add_argument('--workloadModel', nargs='?',
                        type=str, default="synthetic")
    parser.add_argument('--serviceTimeModel', nargs='?',
                        type=str, default="constant")
    parser.add_argument('--serviceTimeParam', nargs='?',
                        type=float, default=1)
    parser.add_argument('--reqCostModel', nargs='?',
                        type=str, default="ec2")
    parser.add_argument('--reqCostParam', nargs='?',
                        type=float, default=0)
    parser.add_argument('--replicationFactor', nargs='?',
                        type=int, default=1)
    parser.add_argument('--shadowReadRatio', nargs='?',
                        type=float, default=0.10)
    parser.add_argument('--accessPatternModel', nargs='?',
                        type=str, default="uniform")
    parser.add_argument('--accessPatternParam', nargs='?',
                        type=float, default=1.1)
    parser.add_argument('--nwLatencyBase', nargs='?',
                        type=float, default=0.960)
    parser.add_argument('--nwLatencyMu', nargs='?',
                        type=float, default=0.040)
    parser.add_argument('--nwLatencySigma', nargs='?',
                        type=float, default=0.0)
    parser.add_argument('--expPrefix', nargs='?',
                        type=str, default="")
    parser.add_argument('--seed', nargs='?',
                        type=int, default=25072014)
    parser.add_argument('--simulationDuration', nargs='?',
                        type=int, default=500)
    parser.add_argument('--numRequests', nargs='?',
                        type=int, default=100)
    parser.add_argument('--logFolder', nargs='?',
                        type=str, default="logs")
    parser.add_argument('--expScenario', nargs='?',
                        type=str, default="")
    parser.add_argument('--logLevel', nargs='?',
                        type=str, default="INFO")
    parser.add_argument('--logFile', nargs='?',
                        type=str, default="stdout")
    parser.add_argument('--demandSkew', nargs='?',
                        type=float, default=0)
    parser.add_argument('--highDemandFraction', nargs='?',
                        type=float, default=0)
    parser.add_argument('--slowServerFraction', nargs='?',
                        type=float, default=0)
    parser.add_argument('--slowServerSlowness', nargs='?',
                        type=float, default=0)
    parser.add_argument('--intervalParam', nargs='?',
                        type=float, default=0.0)
    parser.add_argument('--timeVaryingDrift', nargs='?',
                        type=float, default=0.0)
    parser.add_argument('--batchSizeModel', nargs='?',
                        type=str, default="constant")
    parser.add_argument('--batchSizeParam', nargs='?',
                        type=int, default=1)
    parser.add_argument('--leaderBalanceModel', nargs='?',
                        type=str, default="RR")
    parser.add_argument('--workloadTrace', nargs='?',
                        type=str, default="")
    parser.add_argument('--ctrlInterval', nargs='?',
                        type=int, default=1000)
    #C3-specific params
    parser.add_argument('--selectionStrategy', nargs='?',
                        type=str, default="pending")
    parser.add_argument('--rateInterval', nargs='?',
                        type=int, default=10)
    parser.add_argument('--cubicC', nargs='?',
                        type=float, default=0.000004)
    parser.add_argument('--cubicSmax', nargs='?',
                        type=float, default=10)
    parser.add_argument('--cubicBeta', nargs='?',
                        type=float, default=0.2)
    parser.add_argument('--hysterisisFactor', nargs='?',
                        type=float, default=2)
    parser.add_argument('--concurrencyWeight', nargs='?',
                        type=int, default=-1.0)
    parser.add_argument('--backpressure', action='store_true',
                        default=False)
    parser.add_argument('--costExponent', nargs='?',
                        type=int, default=3.0)

    args = parser.parse_args()

    directory = "../%s" % (args.logFolder)
    if not os.path.exists(directory):
        os.makedirs(directory)

    numeric_level = getattr(logging, args.logLevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % numeric_level)
    constants.LOG_LEVEL = numeric_level

    logger.init("../" + args.logFolder + "/" + args.logFile, constants.LOG_LEVEL)
    log = logger.getLogger("brb", constants.LOG_LEVEL)
    log.info("---> Log start <---")

    start = time.clock()
    runExperiment(args, log)
    finish = time.clock()
    delta = datetime.timedelta(seconds=(finish - start))
    log.info("Simulation finished in %s" % str(delta))

