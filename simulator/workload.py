import SimPy.Simulation as Simulation
import random
import request
import numpy
import constants
import logger


def finished():
    import global_vars
    return all(w.finished() for w in global_vars.workloadGens)

class Workload(Simulation.Process):

    def __init__(self, id_, clientList, serverList,
                 batchDefList,
                 interArrivalModel, interArrivalParam):
        self.id_ = id_
        self.log = logger.getLogger("workload%d" % id_, constants.LOG_LEVEL)
        self.clientList = clientList
        self.serverList = serverList
        self.batchDefList = batchDefList
        self.interArrivalModel = interArrivalModel
        self.interArrivalParam = interArrivalParam
        self.total = sum(client.demandWeight for client in self.clientList)
        self.subRequestCounter = 0
        Simulation.Process.__init__(self, name='Workload' + str(id_))

    def finished(self):
        return len(self.batchDefList) == 0

    # TODO: also need non-uniform client access
    # Need to pin workload to a client
    def run(self):
        critical = self.log.critical
        
        batchCounter = 0
        while not self.finished():
            yield Simulation.hold, self,

            # Push out a request...
            clientNode = self.weightedChoice()

            batchDef, interarrivalModifier = self.batchDefList.pop(0)
            if batchDef is None:
                break
            batchsize = len(batchDef)
            batch = []
            requestCounter = 0
            for reqDef in batchDef:

                parentRequestId = "%s-Request%s" % (self.id_, str(batchCounter))
                fullId = "%s-%s" % (parentRequestId, str(requestCounter))

                hkey, replicaGroup, cost = reqDef
                req = request.Request(fullId=fullId,
                                    parentId=parentRequestId,
                                    key=hkey,
                                    replicaGroup=replicaGroup,
                                    batchsize=batchsize,
                                    sendingClient=clientNode,
                                    cost=cost)
                batch.append(req)
                requestCounter += 1
                self.subRequestCounter += 1
            clientNode.arrival(batch)
            batchCounter += 1

            # Simulate inter-arrival times for requests
            # Note that for trace workloads, the interarrivalModifier is used to model inter-arrival burstiness in 1-second granularities
            # Within each second, we can then utilize a distribution of our choice to synthesize interarrival burstiness
            if (self.interArrivalModel == "poisson"):
                yield Simulation.hold, self,\
                    numpy.random.poisson(self.interArrivalParam*1.0)
            elif (self.interArrivalModel == "constant"):
                yield Simulation.hold, self, (self.interArrivalParam*1.0)
            else:
                critical("Invalid workload inter-arrival time model")
                assert False

    def weightedChoice(self):
        r = random.uniform(0, self.total)
        upto = 0
        for client in self.clientList:
            if upto + client.demandWeight > r:
                return client
            upto += client.demandWeight
        assert False, "Shouldn't get here"
