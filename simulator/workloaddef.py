import numpy
import random
import md5
import logger
import constants
import cPickle
import os.path
from scipy.stats import genpareto
from collections import defaultdict


def gen_hkey(key):
    m = md5.new()
    m.update(key)
    return long(m.hexdigest(), 16)

class WorkloadDef:

    def __init__(self, numRequests, numRgs, reqCostModel, reqCostParam,
                 accessPatternModel, accessPatternParam,
                 batchSizeModel, batchSizeParam,
                 valueSizeModel, valueSizeParam,
                 logFolder, expPrefix):
        self.log = logger.getLogger("workload", constants.LOG_LEVEL)
        self.workload = []
        self.avgValueSize = 0
        self.avgCost = 0
        self.totCostPerRG = {}
        self.totCountPerRG = {}

        self.numRequests = numRequests
        self.numRgs = numRgs
        self.reqCostModel = reqCostModel
        self.reqCostParam = reqCostParam
        self.accessPatternModel = accessPatternModel
        self.accessPatternParam = accessPatternParam
        self.batchSizeModel = batchSizeModel
        self.batchSizeParam = batchSizeParam
        self.valueSizeModel = valueSizeModel
        self.valueSizeParam = valueSizeParam # TODO: use it
        self.logFolder = logFolder
        self.expPrefix = expPrefix
        self.build()

    def getSlice(self, i, j):
        return self.workload[i:j]

    def build(self):
        assert False

# EC2 Cost function derived from an m3 instance with 56% percent caching
ec2ValueCostFnc = numpy.poly1d(([ -6.43052259e-06,   1.49597736e-01,   5.46088015e+02]))
def ec2CostModel(values, limit=10000):
    # NOTE: divide by 1000 as the EC2 cost function was in microseconds
    return [ec2ValueCostFnc(min(v,limit))/1000.0 for v in values]


# Pareto distributed value sizes derived from Facebook kwy-value store workload
# TODO: test this by geenrating 10^6 samples, plotting a histogram and compare to the distribution in the FB paper
def fbGenValueSize():
    #Probabilities for the first 14 values
    distr_1_14 = {1:0.00583, 2:0.17820, 3:0.09239, 4:0.00018,
                  5:0.02740, 6:0.00065, 7:0.00606, 8:0.00023,
                  9:0.00837, 10:0.00837, 11:0.08989, 12:0.00092,
                  13:0.00326, 14:0.01980}
    p_1_14 = sum(distr_1_14.values())
    choice = random.random()
    if (choice > p_1_14):
        r = 0
        numargs = genpareto.numargs
        [ c ] = [0.348238,]*numargs
        while int(r) <= 14:
            r = genpareto.rvs(c, loc=0, scale=214.476)
        return int(r)

    choice = random.random()
    f_x = 0
    for i in distr_1_14.keys():
        f_x += distr_1_14[i]/p_1_14
        if choice <= f_x:
            r = i
            break
    size = int(r)
    assert size > 0
    return size

# Facebook value size distribution
# param[0]: max value size
# param[1]: # of samples from the underlying pareto distribution (10**4)
def fbValueSizeModel(param = (10000,10000)):
    # TODO: what is the cardinality of value sizes?
    # We use 10^4 as it is above the # of requests we generate in each run
    return [min(fbGenValueSize(), param[0]) for i in xrange(param[1])]

class SyntheticWorkloadDef(WorkloadDef):

    def __init__(self, numRequests, numRgs, reqCostModel, reqCostParam,
                 accessPatternModel, accessPatternParam,
                 batchSizeModel, batchSizeParam,
                 valueSizeModel, valueSizeParam,
                 logFolder, expPrefix):
        WorkloadDef.__init__(self, numRequests, numRgs, reqCostModel, reqCostParam,
                 accessPatternModel, accessPatternParam,
                 batchSizeModel, batchSizeParam,
                 valueSizeModel, valueSizeParam,
                 logFolder, expPrefix)

    def build(self):
        info = self.log.info
        critical = self.log.critical

        # Generate value size list
        if self.valueSizeModel == "constant":
            valueSizeList = [1]
        elif self.valueSizeModel == "fb.pareto":
            valueSizeList = fbValueSizeModel()
        else:
            critical("Invalid valueSizeModel")
            assert False


        # Map values to costs
        if self.reqCostModel == "ec2":
            costList = ec2CostModel(valueSizeList, 10000)
        elif self.reqCostModel == "asis":
            costList = valueSizeList
        else:
            critical("Invalid reqCostModel")
            assert False

        # Generate the batch list
        batchDefList = []
        totCostPerRG = defaultdict(int)
        totCountPerRG = defaultdict(int)
        requestCostFD = open("../%s/%s_requestCost" %
                                (self.logFolder,
                                self.expPrefix), 'w')
        rgBatchPopularityFD = open("../%s/%s_RGBatchPopularity" %
                                (self.logFolder,
                                self.expPrefix), 'w')

        rgBatchPopularity = {rg:0.0 for rg in range(self.numRgs)}
        for i in range(self.numRequests):
            if (self.batchSizeModel == "constant"):
                batchsize = self.batchSizeParam
            elif (self.batchSizeModel == "random.expovariate"):
                # TODO, not sure what an actual model looks like
                rand = random.expovariate(1/float(self.batchSizeParam))
                batchsize = max(1, int(rand))
            elif (self.batchSizeModel == "workloadSC"):
                assert False
            else:
                critical("Invalid batch-size-model")
                assert False

            batchDef = []
            rgExists = {rg:False for rg in range(self.numRgs)}
            for j in range(batchsize):
                if (self.accessPatternModel == "uniform"):
                    hkey = gen_hkey("%d-%d" % (i,j))
                    replicaGroup = random.randint(0, self.numRgs - 1)
                elif (self.accessPatternModel == "zipfian"):
                    zipfParam = self.accessPatternParam
                    key = str(numpy.random.zipf(zipfParam))
                    hkey = gen_hkey(key)
                    replicaGroup = hkey % self.numRgs
                else:
                    critical("Invalid access pattern")
                    assert False
                cost = costList[hkey % len(costList)]
                rgExists[replicaGroup] = True
                requestCostFD.write("%d %d\n"%(replicaGroup, cost))
                reqDef = (hkey, replicaGroup, cost)
                batchDef.append(reqDef)
                totCostPerRG[replicaGroup] += cost
                totCountPerRG[replicaGroup] += 1

            #batchDefList format: (batchDef, interarrivalModifier)
            #interarrivalModifiers are set to 1 as they're only relevant for trace workloads
            batchDefList.append((batchDef,1))
            for rg in rgBatchPopularity.keys():
                if rgExists[rg]:
                    rgBatchPopularity[rg] += 1

        rgBatchPopularity = {k: v/self.numRequests*100.0 for k, v in rgBatchPopularity.items()}
        rgLoad = {k: v/sum(totCostPerRG.values())*100.0 for k, v in totCostPerRG.items()}
        for rg, p in rgBatchPopularity.items():
            rgBatchPopularityFD.write("%d %f"%(rg, p))
        rgBatchPopularityFD.close()
        requestCostFD.close()

        self.workload = batchDefList
        self.avgValueSize = numpy.mean(valueSizeList)
        self.avgCost = numpy.mean(costList)
        self.totCostPerRG = totCostPerRG
        self.totCountPerRG = totCountPerRG

        info("---- RG Popularity ----")
        for entry in rgBatchPopularity.items():
            info("RG:%d Batch Presence: %f" %(entry[0], entry[1]))
        info("---- RG Load ----")
        for entry in rgLoad.items():
            info("RG:%d Load: %f" %(entry[0], entry[1]))            

class TraceWorkloadDef(WorkloadDef):

    @staticmethod
    def load_pickle(filename):
        f = open(filename,"rb")
        p = cPickle.load(f)
        f.close()
        return p

    @staticmethod
    def save_pickle(filename, data):
        f = open(filename,"wb")
        cPickle.dump(data, f)
        f.close()

    class Slice():
        def __init__(self, workloadDef, costList, seed):
            self.workloadDef = workloadDef
            self.traceFile = open(workloadDef.traceFile, 'r')
            self.costList = costList
            #Use a non-shared instance of random
            #This is done so that stat calculations match the utilized workload
            self.rand = random.Random(seed)
        def pop(self, i):
            try:
                line = self.traceFile.next()
                line = line.rstrip("\n")
                task = line.split(" ")
                task.pop(0) # Remove hash
                task.pop(0) # Remove timestamp
                #TODO We're not using the IAModifier for now set to 1 instead
                #iAModifier = task.pop(len(task)-1) #Remove last element
                iAModifier = 1
                batchDef = []
                for key in task:
                    key = key.split("|")[0]
                    hkey = gen_hkey(key)
                    replicaGroup = self.rand.randint(0, self.workloadDef.numRgs - 1)
                    cost = self.costList[hkey % len(self.costList)]
                    reqDef = (hkey, replicaGroup, cost)
                    batchDef.append(reqDef)
                return batchDef, float(iAModifier)

            except StopIteration:
                self.traceFile.close()
                return None, None

        def __len__(self):
            if self.traceFile.closed:
                return 0
            else:
                return 1


    def __init__(self, traceFile, numRequests, numRgs, reqCostModel, reqCostParam,
                 accessPatternModel, accessPatternParam,
                 batchSizeModel, batchSizeParam,
                 valueSizeModel, valueSizeParam,
                 logFolder, expPrefix, seed):
        self.traceFile = traceFile
        self.slice = None
        self.sliced = False
        self.reqCost = None
        self.costList = None
        self.seed = seed
        numRequests = 0
        batchSizeParam = 0
        WorkloadDef.__init__(self, numRequests, numRgs, reqCostModel, reqCostParam,
                 accessPatternModel, accessPatternParam,
                 batchSizeModel, batchSizeParam,
                 valueSizeModel, valueSizeParam,
                 logFolder, expPrefix)

    def build(self):
        info = self.log.info
        critical = self.log.critical
        
        #assert self.valueSizeModel == "constant"
        assert self.accessPatternModel == "uniform"

        # Generate value size list
        if self.valueSizeModel == "constant":
            valueSizeList = [1]
        elif self.valueSizeModel == "fb.pareto":
            valueSizeList = fbValueSizeModel()
        else:
            critical("Invalid valueSizeModel")
            assert False


        # Map values to costs
        if self.reqCostModel == "ec2":
            self.costList = ec2CostModel(valueSizeList, 10000)
        elif self.reqCostModel == "asis":
            self.costList = valueSizeList
        else:
            critical("Invalid reqCostModel")
            assert False

        #self.avgValueSize = self.valueSizeParam
        #self.avgCost = self.reqCost
        
        if(os.path.isfile(self.traceFile+'_stats')):
            #Read from stored file
            self.numRequests, self.batchSizeParam, self.avgValueSize, self.avgCost,\
            self.totCostPerRG, self.totCountPerRG = self.load_pickle(self.traceFile+'_stats')
        else:
            workload = TraceWorkloadDef.Slice(self, self.costList, self.seed)
            
            #Calculate trace stats if they are not available
            # Generate the batch list
            totCostPerRG = defaultdict(int)
            totCountPerRG = defaultdict(int)
            requestCostFD = open("../%s/%s_requestCost" %
                                    (self.logFolder,
                                    self.expPrefix), 'w')
            rgBatchPopularityFD = open("../%s/%s_RGBatchPopularity" %
                                    (self.logFolder,
                                    self.expPrefix), 'w')
    
            rgBatchPopularity = {rg:0.0 for rg in range(self.numRgs)}
            while len(workload)>0:
                batch, iAModifier = workload.pop(0)
                if(batch is None):
                    break
                self.numRequests += 1
                self.batchSizeParam += len(batch)
                rgExists = {rg:False for rg in range(self.numRgs)}
                for req in batch:
                    rgExists[req[1]] = True
                    requestCostFD.write("%d %d\n"%(req[1], req[2]))
                    totCostPerRG[req[1]] += req[2]
                    totCountPerRG[req[1]] += 1
                for rg in rgBatchPopularity.keys():
                    if rgExists[rg]:
                        rgBatchPopularity[rg] += 1
    
            rgBatchPopularity = {k: v/self.numRequests*100 for k, v in rgBatchPopularity.items()}
            rgLoad = {k: v/sum(totCostPerRG.values())*100.0 for k, v in totCostPerRG.items()}
            for rg, p in rgBatchPopularity.items():
                rgBatchPopularityFD.write("%d %f"%(rg, p))
            rgBatchPopularityFD.close()
            requestCostFD.close()
            self.avgValueSize = numpy.mean(valueSizeList)
            self.avgCost = numpy.mean(self.costList)
            self.batchSizeParam = self.batchSizeParam/float(self.numRequests)
            self.totCostPerRG = totCostPerRG
            self.totCountPerRG = totCountPerRG
            self.save_pickle(self.traceFile+'_stats', (self.numRequests,
                                                       self.batchSizeParam,
                                                       self.avgValueSize,
                                                       self.avgCost,
                                                       self.totCostPerRG,
                                                       self.totCountPerRG))
            info("---- RG Popularity ----")
            for entry in rgBatchPopularity.items():
                info("RG:%d Batch Presence: %f" %(entry[0], entry[1]))
            info("---- RG Load ----")
            for entry in rgLoad.items():
                info("RG:%d Load: %f" %(entry[0], entry[1]))  
        
        #(re)initialize workload
        self.slice = TraceWorkloadDef.Slice(self, self.costList, self.seed)

    def getSlice(self, i, j):
        if self.sliced:
            assert False, "Only slide once"
        self.sliced = True
        return self.slice
