import SimPy.Simulation as Simulation
import ewma
import constants
import logger
import random
import math
import workload
from collections import defaultdict

class CapacityEstimator():
    def __init__(self, interval, srv, stable_thold, log):
        self.log = log
        self.interval = interval
        self.server = srv
        self.stable_thold = stable_thold
        self.alpha = 0.2

        self.minW = 0.9 * self.interval * srv.resourceCapacity / srv.serviceTime
        self.maxW = 1.1 * self.interval * srv.resourceCapacity / srv.serviceTime
        self.outstandingStable = 0
        self.capacity = (self.minW + self.maxW) / 2.0

        self.state = 'binary'

    def getCapacity(self):
        return self.capacity

    def update(self, congestion, outstanding):
        debug = self.log.debug
        
        old_capacity = self.capacity
        if congestion == False:
            if (self.maxW - self.minW) / self.maxW < self.stable_thold:
                if self.state == 'binary':
                    self.outstandingStable = outstanding
                    self.state = 'stable'
                self.capacity = max(0.0, old_capacity - self.alpha * (outstanding - self.outstandingStable))
            else:
                self.state = 'binary'
                self.capacity = (self.minW + self.maxW) / 2.0
                self.minW = old_capacity
        else:
            self.capacity = self.minW
            self.maxW = old_capacity

        assert self.capacity >= 0.0
        debug("old capacity %f -> new capacity %f", old_capacity, self.capacity)

class Controller(Simulation.Process):

    def __init__(self, interval, servers, clients, serversByReplicaGroup):
        self.id = 'Controller'
        self.log = logger.getLogger(self.id, constants.LOG_LEVEL)
        self.log.debug("Create controller")

        self.clients = clients
        self.servers = servers
        self.serversByReplicaGroup = serversByReplicaGroup

        self.interval = interval # Measurement interval T
        self.previousDemandUpdate = Simulation.now()
        self.previousCongestionUpdate = Simulation.now()
        self.previousCapacityUpdate = Simulation.now()

        self.minReserved = 0.05 # Reserve 5% server capacity to distribute uniformly

        self.feedbackMap = {}

        self.demands = {} # demands[ (i,k) ]: Demand estimate in credits for client i to replica group k
        for cli in clients:
            for rg in serversByReplicaGroup.iterkeys():
                self.demands[ (cli,rg) ] = ewma.EWMA()

        self.congestionSignals = {srv: False for srv in servers}

        self.capacities = {} # capacities[ j ]: Capacity estimate of server j
        # Initialize capacities to T * \mu * parallelism
        for srv in servers:
            self.capacities[srv] = CapacityEstimator(self.interval, srv, 0.01, self.log)

        self.outstandings = self.init_outstandings()

        Simulation.Process.__init__(self, name='Controller')
        Simulation.activate(self, self.run(), Simulation.now())

    def init_outstandings(self):
        return {srv: 0 for srv in self.servers}

    def enqueueDemandFeedback(self, cli, utilizations, backlogs, outstandings):
        data = (cli, utilizations, backlogs, outstandings)
        # self.log.debug("append demand feedback")
        self.feedbackMap[(cli, 'demand')] = data

    def enqueueCongestionFeedback(self, srv, throughput, utilizations):
        data = (srv, throughput, utilizations)
        # self.log.debug("append congestion feedback")
        self.feedbackMap[(srv, 'congestion')] = data

    def updateDemandEstimations(self, cli, utilizations, backlogs, outstandings):
        # now = Simulation.now()
        # expected = self.previousDemandUpdate + self.interval
        # if now != expected:
        #     self.log.info("now %f" % now)
        #     self.log.info("expected %f" % expected)
        #     self.log.info("previousDemandUpdate %f" % self.previousDemandUpdate)
        #     self.log.info("interval %f" % self.interval)
        # assert now == expected

        # self.log.debug(self.demands)
        for rg in self.serversByReplicaGroup.iterkeys():
            d = utilizations[rg] + backlogs[rg]
            self.demands[ (cli,rg) ].update(d)

        for srv, count in outstandings.iteritems():
            self.outstandings[srv] += count
        # self.previousDemandUpdate = Simulation.now()

    def updateCongestionSignals(self, srv, throughput, utilizations):
        # assert Simulation.now() == self.previousCongestionUpdate + self.interval
        tot_utilization = sum(utilizations.itervalues())
        self.congestionSignals[srv] = throughput < (tot_utilization / float(self.interval))
        # self.previousCongestionUpdate = Simulation.now()

    def updateCapacityEstimations(self):
        # assert Simulation.now() == self.previousCapacityUpdate + self.interval
        # self.previousCapacityUpdate = Simulation.now()

        for srv in self.servers:
            self.capacities[srv].update(self.congestionSignals[srv], self.outstandings[srv])

    def creditsAllocation(self):
        debug = self.log.debug
        a = defaultdict(int) # a[ (i,j) ]: Current allocation of credits for client i to server j
        b = defaultdict(int) # b[ k ]: Current cumulative allocation of credits to satisfy demand towards RG k
        g = defaultdict(int) # g[ j ]: Current consumed capacity of server j

        capacityInflation = 1.0 + self.minReserved

        # Distribute uniformly a minimal reserved allocation of credits
        for i in self.clients:
            for j in self.servers:
                capacity = self.capacities[j].getCapacity()
                inc = self.minReserved * capacity / len(self.clients)
                a[ (i,j) ] += inc
                g[j] += inc

        # for i in self.clients:
        #     for k in self.serversByReplicaGroup.iterkeys():
        #         demand_i_k = math.ceil(self.demands[ (i,k) ].getMean())
        #         if demand_i_k > 0.0:
        #             debug("%s has demand %f to RG %d" % (i,demand_i_k,k))

        # debug(self.demands)
        # debug(self.capacities)
        increase = True
        while increase:
            increase = False

            # NOTE: since ordering matters once the demand cannot be completely satisfied, we apply some
            # randomization to avoid introducing a consistent bias in the allocation
            clients = self.clients[:]
            random.shuffle(clients)
            rgs = self.serversByReplicaGroup.keys()
            random.shuffle(rgs)

            for i in clients:
                for k in rgs:
                    servers = self.serversByReplicaGroup[k][:]
                    random.shuffle(servers)

                    demand_i_k = math.ceil(self.demands[ (i,k) ].getMean())
                    if demand_i_k > 0:
                        # debug("%s has demand %f to RG %d" % (i,demand_i_k,k))
                        unsatisfied = demand_i_k - sum([g[j] for j in servers])
                        # debug("unsatisfied %f" % unsatisfied)
                        if unsatisfied > 0:
                            # TODO: could add a weight that increases at every iteration of the while
                            # loop to control what percentage of unsatisfied we are going to allocate
                            # during this iteration of the while loop
                            frac = unsatisfied / float(len(servers))
                            # debug("frac %f" % frac)
                            for j in servers:
                                # debug("demand %s %d = %f" % (i,k,demand_i_k))
                                capacity = capacityInflation * self.capacities[j].getCapacity()
                                if g[j] < capacity and b[k] < demand_i_k:
                                    inc = min(frac, capacity - g[j])
                                    # debug("alloc %f credits to %s %s" % (inc,i,j))
                                    a[ (i,j) ] += inc
                                    b[k] += inc
                                    g[j] += inc
                                    increase = True

        # Allocate remaining capacity proportionally to demand
        # FIXME: apparently we can still deadlock if the request costs are way beyond the tokens that get allocated
        proportions = defaultdict(float) # p[ (i,j)]: Proportionality vector of the demand from client i to server j

        for i in self.clients:
            for k in self.serversByReplicaGroup.iterkeys():
                demand_i_k = max(1, math.ceil(self.demands[ (i,k) ].getMean()))
                servers = self.serversByReplicaGroup[k]
                frac = demand_i_k / float(len(servers))
                for j in servers:
                    proportions[ (i,j) ] += frac

        totDemandPerServer = {}
        for j in self.servers:
            p = [math.ceil(proportions[ (i,j) ]) for i in self.clients ]
            totDemandPerServer[j] = max(1, sum(p))
        # debug(totDemandPerServer)

        for i,j in proportions.iterkeys():
            proportions[ (i,j) ] /= totDemandPerServer[j]

        # debug(proportions)
        for j in self.servers:
            capacity = capacityInflation * self.capacities[j].getCapacity()
            remaining = capacity - g[j]
            # debug("%s has %f remaining capacity of %f" % (j,remaining,capacity))
            if remaining > 0:
                for i in self.clients:
                    if g[j] < capacity:
                        inc = proportions[ (i,j) ] * remaining
                        # debug("giving %f credits to %s" % (inc, i))
                        a[ (i,j) ] += inc
                        g[j] += inc

        for j in self.servers:
            capacity = capacityInflation * self.capacities[j].getCapacity()
            if g[j] < 0.9 * capacity:
                self.log.critical("%s capacity is not fully utilized: %f used of %f" % (j, g[j], capacity))
                assert False

        for x, creds in a.iteritems():
            i, j = x
            # debug("allocation %s to %s: %f credits" % (i, j, creds))
            if round(creds, 9) == 0 and round(self.capacities[j].getCapacity(), 9) > 0:
                self.log.critical("allocation %s to %s: %f credits" % (i, j, creds))
                self.log.critical("0 credits!")
                for rg in self.serversByReplicaGroup.iterkeys():
                    self.log.critical("demand of %s to rg %d was %f" % (i, rg, self.demands[ (i,rg) ].getMean()))
                assert False

        # TODO: monitor events of excessive demands, that is, when some demand remains unallocated
        return a

    def run(self):
        # Every interval recompute the credit allocations and send to the clients
        debug = self.log.debug
        info = self.log.info
        critical = self.log.critical

        while (workload.finished() == False):
            yield Simulation.hold, self, self.interval

            if len(self.feedbackMap) > 0:
                self.outstandings = self.init_outstandings()
                for (sender, feedback), data in self.feedbackMap.items():
                    if feedback == 'demand':
                        self.updateDemandEstimations(*data)
                    elif feedback == 'congestion':
                        self.updateCongestionSignals(*data)
                    else:
                        critical('Invalid feedback type %s' % feedback)
                        assert False
            self.updateCapacityEstimations()

            a = self.creditsAllocation()

            for cli in self.clients:
                creds = {}
                for srv in self.servers:
                    creds[srv] = a[ (cli,srv) ]

                delay = constants.NW_LATENCY_BASE + \
                    random.normalvariate(constants.NW_LATENCY_MU,
                                         constants.NW_LATENCY_SIGMA)
                deliverCredits = DeliverCreditsWithDelay()
                Simulation.activate(deliverCredits,
                                    deliverCredits.run(creds,
                                                        delay,
                                                        cli),
                                    at=Simulation.now())


class DeliverCreditsWithDelay(Simulation.Process):
    def __init__(self):
        Simulation.Process.__init__(self, name='DeliverCreditsWithDelay')

    def run(self, creds, delay, cli):
        yield Simulation.hold, self, delay
        cli.updateCredits(creds)

class DeliverDemandFeedbackWithDelay(Simulation.Process):
    def __init__(self):
        Simulation.Process.__init__(self, name='DeliverDemandFeedbackWithDelay')

    def run(self, cli, utilizations, backlogs, outstandings, delay, ctrl):
        yield Simulation.hold, self, delay
        ctrl.enqueueDemandFeedback(cli, utilizations, backlogs, outstandings)

class DeliverCongestionFeedbackWithDelay(Simulation.Process):
    def __init__(self):
        Simulation.Process.__init__(self, name='DeliverCongestionFeedbackWithDelay')

    def run(self, srv, throughput, utilizations, delay, ctrl):
        yield Simulation.hold, self, delay
        ctrl.enqueueCongestionFeedback(srv, throughput, utilizations)
