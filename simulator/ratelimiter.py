import SimPy.Simulation as Simulation

class RateLimiter():
    def __init__(self, id_, client, rate, rateInterval):
        self.id = id_
        self.rate = rate
        self.lastSent = 0
        self.client = client
        self.tokens = rate
        self.rateInterval = rateInterval
        # TODO: what is the purpose of having maxTokens here?
        # self.maxTokens = ...

    def __repr__(self):
        return "%s: rate:%f rateInterval:%f lastSeen:%f tokens:%f getTokens(now):%f" %\
               (self.id, self.rate, self.rateInterval, self.lastSent, self.tokens,
                self.getTokens(Simulation.now()))

    def update(self, tokens):
        self.lastSent = Simulation.now()
        self.tokens = tokens
        self.rate = tokens

    def acquire(self, cost):
        self.lastSent = Simulation.now()
        self.tokens = max(0, self.tokens - cost)

    def tryAcquire(self, cost):
        tokens = self.getTokens(Simulation.now())
        # NOTE: here we need to round these numbers for the comparison otherwise due to
        # floating point arithmetic precision the function might return a timeToWait
        # equal to zero, which causes the simulator to never advance
        if (round(tokens, 9) >= round(cost, 9)):
            self.tokens = tokens
            return (True, None, None)
        else:
            if(self.rate == 0):
                #FIXME should properly define the maximum waiting time
                return (False, self.rateInterval, self.tokens)
            timeToWait = (cost - tokens) * self.rateInterval/self.rate
            return (False, timeToWait, self.getTokens(Simulation.now() + timeToWait))

    def getTokens(self, when):
        return self.tokens + self.rate/float(self.rateInterval) * (when - self.lastSent)
